#!/bin/bash

# Wait for l1p5 website to start inside container

# @author  Fabrice Jammes

set -euox pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)

. $DIR/include.sh

load_conf

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message

Wait for l1p5 website and database to start inside containers $CONTAINER_L1P5 and $CONTAINER_L1P5_DB.
EOD
}

while ! docker exec -- "$CONTAINER_L1P5" curl -connect-timeout=2 http://localhost:80 >& /dev/null
do
    echo "Wait for l1p5 website to be reachable on port 80"
    docker logs "$CONTAINER_L1P5" -n 50
    # don't spam the server
    sleep 5
done

echo "Succeed in reaching l1p5 website on port 80"

if [ $EXTERNAL_DB_SOCKET = true ]; then
    exit
fi

while ! docker exec -- "$CONTAINER_L1P5_DB" mysql --protocol=SOCKET -u  "$DATABASE_USER" -p"$DATABASE_PASSWORD" -e "select version()" >& /dev/null
do
    echo "Wait for mariadb database to start"
    sleep 2
done

echo "Succeed in reaching mariadb database"


while ! docker exec -- "$CONTAINER_L1P5_DB" mysql --protocol=SOCKET -u  "$DATABASE_USER" -p"$DATABASE_PASSWORD" -e "USE '$DATABASE_NAME';"
do
    echo "Wait for $DATABASE_NAME database to be created"
    sleep 2
done

echo "Succeed in using $DATABASE_NAME database"

echo "FIXME wait for mysql to warm up"
sleep 5

# while ! docker exec -- "$CONTAINER_L1P5" python3 manage.py migrate --plan >& /dev/null
# do
#    echo "Wait for database migration planning to succeed"
#     sleep 1
# done

# while ! docker exec -- "$CONTAINER_L1P5" python3 manage.py migrate --fake >& /dev/null
# do
#     echo "Wait for fake database migration to succeed"
#     sleep 1
# done

echo "l1p5 website and database are up and running"
