[![CI status](https://framagit.org/labos1point5/l1p5-vuejs/badges/master/pipeline.svg)](https://framagit.org/labos1point5/l1p5-vuejs/-/pipelines)
[![GPLv3 or later](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Chat](https://img.shields.io/badge/mattermost-labos1point5-blueviolet)](https://team.picasoft.net/labos-1point5/channels/town-square)
[![SWH](https://archive.softwareheritage.org/badge/origin/https://framagit.org/labos1point5/l1p5-vuejs.git/)](https://archive.softwareheritage.org/browse/origin/?origin_url=https://framagit.org/labos1point5/l1p5-vuejs.git)


# General information

This documentation is the **developper**'s documentation of the tools developed by [Labos 1point5](https://labos1point5.org/) and available as a web app.

There are licensed under GPL-v3 or later.

[[_TOC_]]

# Architecture

* Frontend: VueJS (nodejs)
* Backend: Django (REST API) (python3)

# Getting started with Docker compose

## Requirements

* Docker
* Docker compose

## Configuration

The configuration file `docker-compose.yml` has all of the configuration for the container's mount points and port forwarding. A separate database container is run in parallel and has preconfigured persistant volumes.
By default the following ports are available on the host machine:

* 7000 - The production build for the service running a WSGI process and Apache web server 
* 8080 - The vue.js development server, this will auto reload after any changes are made to front end code 
* 8000 - Django development server, will also auto reload with backend changes. This acts as the API port for development server

## Commands 

### Start the services
`docker compose up` = This will run in the foreground and will dump all stdout/stderr to the terminal 

`docker compose up -d` = This will run the services as a daemon in the background

### Stop the services 
`Ctrl-c` if running in the foreground 

`docker compose down` if in the background or from another terminal in the same directory as docker-compose.yml

### CLI 
When a service/container is already running `container` is the service name in docker-compose.yml

`docker compose exec <container> <command>` for example: `docker compose exec webserver bash`

When a service/container is not running:

`docker compose run <container> <command>`

## Logging 
Logs are in their normal places within the containers, but they are also piped to a local `log` directory mounted into the working directory of the webserver container. This is to ease debugging by accessing directly from the host. 

## Building 
Sometimes re-building the container becomes necessary i.e. a new package is added to the base image, or a python module is added to the backend server etc.

`docker compose build webserver` - will rebuild the image using the Dockerfile.

# Manual Installation

To get ready with the development you'll need to have `NodeJS`, `Python3` and all their dependencies and a database.

## NodeJS

As a developper using a node version  manager like [nvm](https://github.com/nvm-sh/nvm) is encouraged.

In developement node version: 16

```bash
npm install
```

## Python3 environment

As a developper using a virtual environment for python is a good practice. We suggest to use the standard [`virtualenv`](https://docs.python.org/3/library/venv.html) or [`pyenv-virtualenv`](https://github.com/pyenv/pyenv-virtualenv).

Dependencies are managed using `pip` however some extra system packages might be
necessary:

On Debian and derivatives ```python3-dev default-libmysqlclient-dev build-essential pkg-config```  (This is required to build `mysqclient` pip package)


```bash
# example with virtualenv
python3 -m venv [nom de du virtualenv]
source [nom du virtualenv]/bin/activate
pip install -r rootfs/opt/l1p5/requirements.txt
```

## Database

For development purpose, one might use `sqlite3` or `MariaDB` as storage backend for Django.

### SQLite3

On Debian and derivatives, install the `sqlite3` package.  To use `sqlite3` as a
database, you need to set the environment variable `DATABASE_ENGINE` to
`django.db.backends.sqlite3`.

- Run the migration: `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py migrate`
- Run the server dev server: `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py runserver`

### MariaDB

On Debian and derivatives, install the `mariadb-server` package.
As MariaDB is the default database backend in `default.py` you can simply run the following

- Initialize the database (first time only):  `sudo mysql < mariadb/init.sql`
- Run the tests against MariaDB:  `python3 manage.py test`
- Run the migrations:  `python3 manage.py migrate`

## Start the servers

```bash
cd homefs
npm run serve
python manage.py runserver
```

# Code style

We don't enforce (yet) coding style convention automatically but the following
is what we mainly use:

- javascript: prettier + lint
```bash
npx prettier --no-semi --single-quote --trailing-comma none  --write  <path to files or directory> && npm run lint
```

- python: black
```bash
black <path to file or directory to prettify>
```

# Tests

Check `.gitlab-ci.yml`.

- Backend unit test:
    - (sqlite3) `DATABASE_ENGINE=django.db.backends.sqlite3 python3 manage.py test`
    - (mariaDB) `python3 manage.py test`
- Frontend unit test:
    - `npm test`

- Functionnal tests ([playwright](https://playwright.dev/python/))
    - `./tests/runner.sh` (requires Docker)
    - or `npx playwright test`

- On tags (only), the docker image will be build and pushed on Inria registry
(see the [mirror](https://gitlab.inria.fr/l1p5/l1p5-vuejs)).

# Populate with some users/data

Check the  `python manage.py populate` command.

# Other docs

* Django : [https://www.djangoproject.com](https://www.djangoproject.com/)
* MariaDB : [https://devdocs.io/mariadb/](https://devdocs.io/mariadb/)
* VueJS : [https://vuejs.org/guide/introduction.html](https://vuejs.org/guide/introduction.html)
* [Docker startup (prod use)](./doc/docker.md)
