#!/bin/bash

# Run docker containers containing l1p5 website and database

# @author  Fabrice Jammes

set -euo pipefail

DIR=$(cd "$(dirname "$0")"; pwd -P)
. $DIR/include.sh
load_conf

DJANGO_SETTINGS_FILE="$DIR/homefs/django-settings.txt"

usage() {
    cat << EOD
Usage: $(basename "$0") [options]
Available options:
  -h            This message

Generate Django settings from $DIR/conf.sh
EOD
}

# Get the options
while getopts h c ; do
    case $c in
        h) usage ; exit 0 ;;
        \?) usage ; exit 2 ;;
    esac
done
shift "$((OPTIND-1))"

if [ $# -ne 0 ] ; then
    usage
    exit 2
fi

if [ "$EXTERNAL_DB_SOCKET" = false ]; then
	DATABASE_HOST="/run/mysqld/mysqld.sock"
fi

echo "Use $DIR/conf.sh to generate Django settings to $DJANGO_SETTINGS_FILE"
cat <<EOF > $DJANGO_SETTINGS_FILE
# Django setting files used by backend/settings/default.py
DATABASE_NAME=$DATABASE_NAME
DATABASE_USER=$DATABASE_USER
DATABASE_PASSWORD=$DATABASE_PASSWORD
DATABASE_HOST=$DATABASE_HOST
DATABASE_PORT=$DATABASE_PORT
DEBUG=$DEBUG
EMAIL_HOST=$EMAIL_HOST
EMAIL_PORT=$EMAIL_PORT
SECRET_KEY=$SECRET_KEY
EOF
