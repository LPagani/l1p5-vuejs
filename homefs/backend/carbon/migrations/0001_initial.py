# Generated by Django 2.2.12 on 2020-04-30 14:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Batiment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=250)),
                ('superficie', models.FloatField()),
                ('occupation', models.FloatField()),
                ('isOwnedByLab', models.BooleanField()),
                ('chauffageElectrique', models.BooleanField()),
                ('typeChauffage', models.CharField(max_length=250)),
                ('reseauChauffageUrbain', models.CharField(max_length=250)),
                ('autoProduction', models.BooleanField()),
                ('autoconsommation', models.IntegerField(null=True)),
                ('site', models.CharField(max_length=250, null=True)),
                ('laboratoire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='batiment', to='api.Laboratoire')),
            ],
        ),
        migrations.CreateModel(
            name='BGES',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('annee', models.IntegerField()),
                ('created', models.DateField(auto_now_add=True)),
                ('nbChercheurs', models.IntegerField(null=True)),
                ('nbEnseignants', models.IntegerField(null=True)),
                ('nbITA', models.IntegerField(null=True)),
                ('nbPostDoc', models.IntegerField(null=True)),
                ('budget', models.IntegerField(null=True)),
                ('laboratoire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='bges', to='api.Laboratoire')),
            ],
        ),
        migrations.CreateModel(
            name='Mission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('villeDepart', models.CharField(max_length=100)),
                ('villeDepartLat', models.DecimalField(decimal_places=7, max_digits=10)),
                ('villeDepartLng', models.DecimalField(decimal_places=7, max_digits=10)),
                ('paysDepart', models.CharField(max_length=100)),
                ('villeDestination', models.CharField(max_length=100)),
                ('villeDestinationLat', models.DecimalField(decimal_places=7, max_digits=10)),
                ('villeDestinationLng', models.DecimalField(decimal_places=7, max_digits=10)),
                ('paysDestination', models.CharField(max_length=100)),
                ('modeDeplacement', models.CharField(max_length=250)),
                ('nbPersonneVoiture', models.IntegerField(null=True)),
                ('motifDeplacement', models.CharField(max_length=250, null=True)),
                ('statut', models.CharField(max_length=250, null=True)),
                ('isAllerRetour', models.BooleanField(null=True)),
                ('nbEffectue', models.IntegerField()),
                ('bges', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='mission', to='carbon.BGES')),
            ],
        ),
        migrations.CreateModel(
            name='Vehicule',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=250)),
                ('typeVehicule', models.CharField(max_length=250)),
                ('motorisation', models.CharField(max_length=250)),
                ('unite', models.CharField(max_length=250)),
                ('puissance', models.IntegerField(null=True)),
                ('nbMoteurs', models.IntegerField(null=True)),
                ('shp', models.IntegerField(null=True)),
                ('controleOperationnel', models.BooleanField()),
                ('laboratoire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='vehicule', to='api.Laboratoire')),
            ],
        ),
        migrations.CreateModel(
            name='IDMisson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=20)),
                ('mission', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='idMission', to='carbon.Mission')),
            ],
        ),
        migrations.CreateModel(
            name='GazRefrigerant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=50)),
                ('cTotale', models.IntegerField(null=True)),
                ('batimentGazRefrigerant', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='consommationGazRefrigerant', to='carbon.Batiment')),
                ('bges', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='consommationGaz', to='carbon.BGES')),
            ],
        ),
        migrations.CreateModel(
            name='DeplacementDT',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('statut', models.CharField(max_length=250)),
                ('nbJourTravail', models.IntegerField()),
                ('nbAllerRetour', models.IntegerField()),
                ('marche', models.IntegerField(null=True)),
                ('velo', models.IntegerField(null=True)),
                ('veloElectrique', models.IntegerField(null=True)),
                ('trotinetteElectrique', models.IntegerField(null=True)),
                ('deRouesMotorise', models.IntegerField(null=True)),
                ('voiture', models.IntegerField(null=True)),
                ('bus', models.IntegerField(null=True)),
                ('tramway', models.IntegerField(null=True)),
                ('train', models.IntegerField(null=True)),
                ('rer', models.IntegerField(null=True)),
                ('metro', models.IntegerField(null=True)),
                ('nbPersonne2roues', models.IntegerField(null=True)),
                ('nbPersonneVoiture', models.IntegerField(null=True)),
                ('motorisation', models.CharField(max_length=25)),
                ('bges', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='deplacementDT', to='carbon.BGES')),
            ],
        ),
        migrations.CreateModel(
            name='Consommation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cJanvier', models.IntegerField(null=True)),
                ('cFevrier', models.IntegerField(null=True)),
                ('cMars', models.IntegerField(null=True)),
                ('cAvril', models.IntegerField(null=True)),
                ('cMai', models.IntegerField(null=True)),
                ('cJuin', models.IntegerField(null=True)),
                ('cJuillet', models.IntegerField(null=True)),
                ('cAout', models.IntegerField(null=True)),
                ('cSeptembre', models.IntegerField(null=True)),
                ('cOctobre', models.IntegerField(null=True)),
                ('cNovembre', models.IntegerField(null=True)),
                ('cDecembre', models.IntegerField(null=True)),
                ('cTotale', models.IntegerField(null=True)),
                ('isMensuelle', models.BooleanField()),
                ('batimentChauffage', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='consommationChauffage', to='carbon.Batiment')),
                ('batimentElectricite', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='consommationElectricite', to='carbon.Batiment')),
                ('bges', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='consommation', to='carbon.BGES')),
                ('vehicule', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='consommation', to='carbon.Vehicule')),
            ],
        ),
    ]
