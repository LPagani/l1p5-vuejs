import io
from typing import Tuple

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client
from django.test import TestCase
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status

from pathlib import Path

from backend.carbon.models import GHGI
from backend.test_utils import ensure_user_created, ensure_lab_created
from backend.users.models import L1P5Group, L1P5User
from backend.transition.models import (
    DRAFT,
    PUBLISHED,
    SUBMITTED,
    PublicAction,
    PublicActionFile,
    Tag,
)


REQUEST_KWARGS = dict(content_type="application/json")


def make_file_data(name="test_file.pdf", content="test content"):
    f = io.StringIO(content)
    # beware file extension is validated
    f.name = name

    return {f.name: f}


def make_authenticated_client(email: str) -> Tuple[L1P5User, Client]:
    user_authenticated = ensure_user_created(email)
    refresh = RefreshToken.for_user(user_authenticated)
    # in django >= 4.2 we could use the headers for that
    api_authenticated = Client(
        HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
    )
    return user_authenticated, api_authenticated


def make_action(laboratory, admin_status):
    # add an action
    action = PublicAction(
        laboratory=laboratory,
        title=f"Test action",
        text=f"Test body action",
    )
    action.save(update_fields="admin_status")
    tag, _ = Tag.objects.get_or_create(descriptor="tag:ges:buildings")
    action.tags.set([tag])
    # purpose: get the status back
    action.admin_status = admin_status
    action.save(update_fields="admin_status")

    # add a file
    uploaded_file = SimpleUploadedFile(
        "test.pdf", b"plop", content_type="application/pdf"
    )
    file = PublicActionFile(action=action, name="test.pdf", file=uploaded_file)
    file.save()
    return action


class PermissionsTestCase(TestCase):
    """Attempt to factorize permissions testing logic.

    This creates 4 api clients
    - one unauthenticated (api_unauthenticated)
    - one authenticated but doesn't own any resource (api_authenticated)
    - one authenticated that own some resources (api_user)
    - one reviewer (api_reviewer)
    - one admin (tbd) (api_admin)

    This will eventually lands in a common place and used by various apps' tests.
    """

    def setUp(self) -> None:
        # unauthenticated
        self.api_unauthenticated = Client()

        # just a user (no resource)
        self.user_authenticated, self.api_authenticated = make_authenticated_client(
            "authenticated@l1p5.org"
        )

        # a user with some resources
        self.user, self.api_user = make_authenticated_client("user@l1p5.org")

        # a reviewer
        self.user_reviewer, self.api_reviewer = make_authenticated_client(
            "reviewer@l1p5.org"
        )
        reviewer_group = L1P5Group.objects.get(name="reviewer")
        self.user_reviewer.groups.set([reviewer_group])
        # refresh permission cache
        self.user_reviewer = L1P5User.objects.get(email=self.user_reviewer.email)

        # an admin
        self.user_admin, self.api_admin = make_authenticated_client("admin@l1p5.org")
        self.user_admin.is_superuser = True
        self.user_admin.save()

    def tearDown(self) -> None:
        self.user_authenticated.delete()
        self.user.delete()
        self.user_reviewer.delete()
        self.user_admin.delete()


class TestPermissions(PermissionsTestCase):
    """Make sure unauthorized client get unexpectedly access to some resources."""

    def setUp(self) -> None:
        super().setUp()
        # create some resource for the user
        self.laboratory = ensure_lab_created(self.user)
        self.ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        self.ghgi.save()

        self.draft_action = make_action(self.laboratory, DRAFT)
        self.submitted_action = make_action(self.laboratory, SUBMITTED)
        self.published_action = make_action(self.laboratory, PUBLISHED)

        # TODO(msimonin): handle a file

    def assert_permissions(
        self,
        url,
        method="get",
        data=None,
        unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
        unauthenticated_cb=None,
        authenticated_rc=status.HTTP_403_FORBIDDEN,
        authenticated_cb=None,
        user_rc=status.HTTP_200_OK,
        user_cb=None,
        reviewer_rc=status.HTTP_200_OK,
        reviewer_cb=None,
        admin_rc=status.HTTP_200_OK,
        admin_cb=None,
        **kwargs,
    ):
        local_kwargs = dict(**kwargs)
        content_type = kwargs.pop("content_type", None)
        if content_type is None:
            local_kwargs.update(content_type="application/json")
        if data is not None:
            # don't send data if there isn't any
            local_kwargs.update(data=data)

        for (client, rc, cb, desc) in zip(
            [
                self.api_unauthenticated,
                self.api_authenticated,
                self.api_user,
                self.api_reviewer,
                self.api_admin,
            ],
            [unauthenticated_rc, authenticated_rc, user_rc, reviewer_rc, admin_rc],
            [unauthenticated_cb, authenticated_cb, user_cb, reviewer_cb, admin_cb],
            ["unauthenticated", "authenticated", "user", "reviewer", "admin"],
        ):
            if rc is None:
                # skip test
                continue
            r = getattr(client, method)(url, **local_kwargs)
            self.assertEqual(
                rc, r.status_code, f"[{desc}] {method} on {url} must returns {rc}"
            )
            if cb is not None:
                self.assertTrue(cb(r))

    def test_mgmt_public_actions(self):
        self.assert_permissions(
            "/api/transition/actions/",
            authenticated_rc=status.HTTP_200_OK,
            # we return None if the user is authenticated but no laboratory
            authenticated_cb=lambda r: (not r.content),
        )

    def test_mgmt_public_action_draft(self):
        # Draft
        self.assert_permissions(
            f"/api/transition/actions/{self.draft_action.id}/",
            method="get",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
        )

        self.assert_permissions(
            f"/api/transition/actions/{self.draft_action.id}/",
            method="post",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
        )

        self.assert_permissions(
            f"/api/transition/actions/{self.draft_action.id}/",
            method="post",
            unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_200_OK,
            # we can't delete twice the action so we don't test the following
            reviewer_rc=None,
            admin_rc=None,
        )

    def test_mgmt_public_action_submitted(self):
        # submitted
        self.assert_permissions(
            f"/api/transition/actions/{self.submitted_action.id}/",
            method="get",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
        )

        self.assert_permissions(
            f"/api/transition/actions/{self.submitted_action.id}/",
            method="post",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
        )

        self.assert_permissions(
            f"/api/transition/actions/{self.submitted_action.id}/",
            method="post",
            unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_200_OK,
            # we can't delete twice the action so we don't test the following
            reviewer_rc=None,
            admin_rc=None,
        )

    def test_mgmt_public_action_published(self):
        # submitted
        self.assert_permissions(
            f"/api/transition/actions/{self.published_action.id}/",
            method="get",
            authenticated_rc=status.HTTP_200_OK,
        )

        self.assert_permissions(
            f"/api/transition/actions/{self.published_action.id}/",
            method="post",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
        )

        self.assert_permissions(
            f"/api/transition/actions/{self.published_action.id}/",
            method="post",
            unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_200_OK,
            # we can't delete twice the action so we don't test the following
            reviewer_rc=None,
            admin_rc=None,
        )

    def test_mgmt_public_file(self):
        self.assert_permissions(
            f"/api/transition/actions/{self.published_action.id}/files/",
            method="post",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_201_CREATED,
            reviewer_rc=status.HTTP_201_CREATED,
            admin_rc=status.HTTP_201_CREATED,
        )

    def test_reviewer_get_quota(self):
        self.assert_permissions(
            f"/api/reviewer/get_transition_quota/{self.laboratory.id}/",
            method="get",
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_200_OK,
            reviewer_rc=status.HTTP_200_OK,
            admin_rc=status.HTTP_200_OK,
        )

    def test_public_actions_published(self):
        # results are paginated
        one_action_cb = lambda r: r.json()["count"] == 1
        self.assert_permissions(
            f"/api/public/actions/",
            unauthenticated_rc=status.HTTP_200_OK,
            unauthenticated_cb=one_action_cb,
            authenticated_rc=status.HTTP_200_OK,
            authenticated_cb=one_action_cb,
            user_rc=status.HTTP_200_OK,
            user_cb=one_action_cb,
            reviewer_rc=status.HTTP_200_OK,
            reviewer_cb=one_action_cb,
            admin_rc=status.HTTP_200_OK,
            admin_cb=one_action_cb,
        )

    def test_public_action_published(self):
        # if it's a draft >user can get it
        self.assert_permissions(
            f"/api/public/actions/{self.draft_action.id}/",
            unauthenticated_rc=status.HTTP_403_FORBIDDEN,
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_200_OK,
            reviewer_rc=status.HTTP_200_OK,
            admin_rc=status.HTTP_200_OK,
        )

        # if it's published anyone can
        self.assert_permissions(
            f"/api/public/actions/{self.published_action.id}/",
            unauthenticated_rc=status.HTTP_200_OK,
            authenticated_rc=status.HTTP_200_OK,
            user_rc=status.HTTP_200_OK,
            reviewer_rc=status.HTTP_200_OK,
            admin_rc=status.HTTP_200_OK,
        )

        # if the action doesn't exist -> 404
        self.assert_permissions(
            f"/api/public/actions/12345/",
            unauthenticated_rc=status.HTTP_404_NOT_FOUND,
            authenticated_rc=status.HTTP_404_NOT_FOUND,
            user_rc=status.HTTP_404_NOT_FOUND,
            reviewer_rc=status.HTTP_404_NOT_FOUND,
            admin_rc=status.HTTP_404_NOT_FOUND,
        )

    def test_public_action_add_file(self):
        data = make_file_data()
        # django uses multipart/form-data by default
        from django.test.client import MULTIPART_CONTENT

        for action in [self.draft_action, self.submitted_action, self.published_action]:
            self.assert_permissions(
                f"/api/transition/actions/{action.id}/files/",
                method="post",
                data=data,
                content_type=MULTIPART_CONTENT,
                # yes that means that a user can add a file on a published action
                user_rc=status.HTTP_201_CREATED,
                reviewer_rc=status.HTTP_201_CREATED,
                admin_rc=status.HTTP_201_CREATED,
            )

    def test_public_action_draft_get_file(self):
        action_id = self.draft_action.id
        # we know there's one file
        file_id = self.draft_action.files.first().id
        self.assert_permissions(f"/api/transition/actions/{action_id}/files/{file_id}/")

    def test_public_action_submitted_get_file(self):
        action_id = self.submitted_action.id
        # we know there's one file
        file_id = self.submitted_action.files.first().id
        self.assert_permissions(f"/api/transition/actions/{action_id}/files/{file_id}/")

    def test_public_action_published_get_file(self):
        action_id = self.published_action.id
        # we know there's one file
        file_id = self.published_action.files.first().id
        self.assert_permissions(
            f"/api/transition/actions/{action_id}/files/{file_id}/",
            unauthenticated_rc=status.HTTP_200_OK,
            authenticated_rc=status.HTTP_200_OK,
        )

    def test_reviewer_actions_submitted(self):
        self.assert_permissions(
            f"/api/reviewer/actions/",
            unauthenticated_rc=status.HTTP_401_UNAUTHORIZED,
            authenticated_rc=status.HTTP_403_FORBIDDEN,
            user_rc=status.HTTP_403_FORBIDDEN,
            reviewer_rc=status.HTTP_200_OK,
            reviewer_cb=lambda r: len(r.json()) == 1,
            admin_rc=status.HTTP_200_OK,
            admin_cb=lambda r: len(r.json()) == 1,
        )

    def test_reviewer_action_get(self):
        # toggle action status
        self.assert_permissions(
            f"/api/reviewer/actions/{self.draft_action.id}/",
            method="post",
            user_rc=status.HTTP_403_FORBIDDEN,
            reviewer_rc=status.HTTP_403_FORBIDDEN,
            admin_rc=status.HTTP_403_FORBIDDEN,
        )
        self.assert_permissions(
            f"/api/reviewer/actions/{self.submitted_action.id}/",
            method="post",
            user_rc=status.HTTP_403_FORBIDDEN,
            reviewer_rc=status.HTTP_200_OK,
            admin_rc=status.HTTP_200_OK,
        )

        self.assert_permissions(
            f"/api/reviewer/actions/{self.published_action.id}/",
            method="post",
            user_rc=status.HTTP_403_FORBIDDEN,
            reviewer_rc=status.HTTP_200_OK,
            admin_rc=status.HTTP_200_OK,
        )

    def test_reviewer_action_put(self):
        self.assert_permissions(
            f"/api/reviewer/actions/{self.draft_action.id}/",
            method="put",
        )
        self.assert_permissions(
            f"/api/reviewer/actions/{self.submitted_action.id}/",
            method="put",
        )

        self.assert_permissions(
            f"/api/reviewer/actions/{self.published_action.id}/",
            method="put",
        )

    def test_reviewer_action_get(self):
        self.assert_permissions(
            f"/api/reviewer/actions/{self.draft_action.id}/",
        )
        self.assert_permissions(
            f"/api/reviewer/actions/{self.submitted_action.id}/",
        )

        self.assert_permissions(
            f"/api/reviewer/actions/{self.published_action.id}/",
        )

    def test_reviewer_get_laboratory(self):
        self.assert_permissions(
            f"/api/reviewer/get_laboratory/{self.laboratory.id}/",
        )

    def test_has_reviewer_permissions(self):
        check_cb = lambda b: lambda r: r.json()["has_reviewer_permissions"] == b
        self.assert_permissions(
            f"/api/transition/has_reviewer_permissions/",
            unauthenticated_rc=status.HTTP_200_OK,
            unauthenticated_cb=check_cb(False),
            authenticated_rc=status.HTTP_200_OK,
            authenticated_cb=check_cb(False),
            user_rc=status.HTTP_200_OK,
            user_cb=check_cb(False),
            reviewer_cb=check_cb(True),
            admin_cb=check_cb(True),
        )

    def test_reviewer_messages_draft(self):
        self.assert_permissions(
            f"/api/reviewer/actions/{self.draft_action.id}/messages/",
        )

        self.assert_permissions(
            f"/api/reviewer/actions/{self.draft_action.id}/messages/",
            method="post",
        )

    def test_reviewer_messages_submitted(self):
        self.assert_permissions(
            f"/api/reviewer/actions/{self.submitted_action.id}/messages/",
        )

        self.assert_permissions(
            f"/api/reviewer/actions/{self.submitted_action.id}/messages/",
            method="post",
        )

    def test_reviewer_messages_published(self):
        self.assert_permissions(
            f"/api/reviewer/actions/{self.published_action.id}/messages/",
        )

        self.assert_permissions(
            f"/api/reviewer/actions/{self.published_action.id}/messages/",
            method="post",
        )


class TestAction(TestCase):
    def setUp(self) -> None:
        # create a user with a ghgi
        self.user = ensure_user_created("test@l1p5.org")
        self.laboratory = ensure_lab_created(self.user)

        refresh = RefreshToken.for_user(self.user)
        # in django >= 4.2 we could use the headers for that
        self.api_user = Client(
            HTTP_AUTHORIZATION=f"Bearer {refresh.access_token}",
        )

    def test_has_submitted_ghgis_not_submitted(self):
        ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
        )
        ghgi.save()

        response = self.api_user.get(
            f"/api/transition/has_submitted_ghgis/", **REQUEST_KWARGS
        )
        r = response.json()

        self.assertFalse(r["has_submitted_ghgis"])

    def test_has_submitted_one_submitted(self):
        modules = {
            "electricity": False,
            "heatings": False,
            "refrigerants": False,
            "water": False,
            "construction": False,
            "commutes": False,
            "travels": False,
            "devices": False,
            "purchases": False,
            "vehicles": False,
        }
        # no ghgi => False
        response = self.api_user.get(
            f"/api/transition/has_submitted_ghgis/", **REQUEST_KWARGS
        )
        r = response.json()
        self.assertFalse(r["has_submitted_ghgis"])

        # no module submitted => False
        ghgi = GHGI(
            laboratory=self.laboratory,
            year=2042,
            nResearcher=1,
            nProfessor=1,
            nEngineer=1,
            nStudent=1,
            budget=int(10e6),
            submitted=modules,
        )
        ghgi.save()
        response = self.api_user.get(
            f"/api/transition/has_submitted_ghgis/", **REQUEST_KWARGS
        )
        r = response.json()
        self.assertFalse(r["has_submitted_ghgis"])

        for module in modules.keys():
            submitted = dict(**modules)
            submitted[module] = True
            ghgi = GHGI(
                laboratory=self.laboratory,
                year=2042,
                nResearcher=1,
                nProfessor=1,
                nEngineer=1,
                nStudent=1,
                budget=int(10e6),
                submitted=submitted,
            )
            ghgi.save()

            response = self.api_user.get(
                f"/api/transition/has_submitted_ghgis/", **REQUEST_KWARGS
            )
            r = response.json()

            self.assertTrue(r["has_submitted_ghgis"])
            ghgi.delete()

    def test_upload_delete_file(self):
        data = dict(title="plop", text="plip")
        response = self.api_user.post(
            f"/api/transition/actions/", data, **REQUEST_KWARGS
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        r = response.json()
        self.assertEqual("plop", r["title"])
        self.assertEqual("plip", r["text"])
        self.assertEqual("adminstatus:draft", r["admin_status"])

        # get the created action
        response = self.api_user.get(f"/api/transition/actions/{r['id']}/")
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        r = response.json()
        self.assertEqual("plop", r["title"])
        self.assertEqual("plip", r["text"])

        # Upload a file
        name = "test.pdf"
        content = "test content"
        data = make_file_data(name=name, content=content)
        # django uses multipart/form-data by default
        from django.test.client import MULTIPART_CONTENT

        response = self.api_user.post(
            f"/api/transition/actions/{r['id']}/files/",
            data,
            content_type=MULTIPART_CONTENT,
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        r = response.json()
        self.assertEqual(1, len(r["files"]))

        # test that the file exist on the fs on the right place
        from django.conf import settings

        filepath = (
            Path(settings.MEDIA_ROOT)
            / f"{self.laboratory.id}"
            / "actions"
            / f"{r['id']}"
            / name
        )
        self.assertTrue(filepath.exists())
        self.assertEqual(content, filepath.read_text())

        # test that the file has been removed from the fs
        response = self.api_user.delete(
            f"/api/transition/actions/{r['id']}/files/{r['files'][0]['id']}/",
            **REQUEST_KWARGS,
        )
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertFalse(filepath.exists())

    def tearDown(self) -> None:
        self.user.delete()
