from django.views.generic import TemplateView
from django.views.decorators.cache import never_cache
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
import copy

from .models import Settings, Laboratory, Administration, Discipline, Site, LaboratoryDiscipline
from .serializers import SettingsSerializer, AdministrationSerializer, DisciplineSerializer, LaboratorySerializer

# Serve Vue Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))

def init_settings(request):
    for setting in request.data.pop('settings'):
        if not Settings.objects.filter(section=setting["section"]).filter(name=setting["name"]).exists():
            Settings.objects.create(**setting)

@api_view(['POST'])
@permission_classes([])
def get_settings(request):
    init_settings(request)
    settings = Settings.objects.all()
    serializer = SettingsSerializer(settings, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['POST'])
def save_settings(request):
    settings = request.data.pop('settings')
    for setting in settings:
        s, created = Settings.objects.update_or_create(id=setting['id'], defaults=setting)
    settings = Settings.objects.all()
    serializer = SettingsSerializer(settings, many=True)
    return Response(serializer.data, status=status.HTTP_201_CREATED)

@api_view(['GET'])
@permission_classes([])
def get_administrations(request):
    administrations = Administration.objects.all()
    serializer = AdministrationSerializer(administrations, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([])
def get_disciplines(request):
    sous_domaines = Discipline.objects.all()
    serializer = DisciplineSerializer(sous_domaines, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def save_laboratory(request):
    if request.user.is_authenticated:
        # sub objects data
        sites_data = request.data.pop('sites')
        administrations_data = request.data.pop('administrations')
        disciplines_data = request.data.pop('disciplines')
        request.data["referent"] = request.user
        # build the laboratory object if exists or create it
        laboratory = Laboratory.objects.filter(referent=request.user)
        lid = None
        if len(laboratory) == 1:
            lid = laboratory[0].id
        laboratory, created = Laboratory.objects.update_or_create(id=lid, defaults=request.data)
        laboratory.administrations.clear()
        for administration_data in administrations_data:
            if (type(administration_data) is str) :
                # Use case insensitive test to check if administration exists or not
                if Administration.objects.filter(name__iexact=administration_data).exists():
                    administration = Administration.objects.get(name__iexact=administration_data)
                    laboratory.administrations.add(administration)
                else:
                    administration = Administration.objects.create(name=administration_data)
                    laboratory.administrations.add(administration)
            else :
                administration = Administration.objects.get(id=administration_data['id'])
                laboratory.administrations.add(administration)
        laboratory.disciplines.clear()
        for discipline_data in disciplines_data:
            d, created = LaboratoryDiscipline.objects.update_or_create(
                discipline_id=discipline_data['id'],
                laboratory_id=laboratory.id,
                percentage=discipline_data['percentage']
            )
        lab_sites = Site.objects.filter(laboratory_id=laboratory.id)
        for lab_site in lab_sites:
            lab_site.delete()
        for site_data in sites_data:
            s = Site.objects.create(name=site_data, laboratory=laboratory)
        serializer = LaboratorySerializer(laboratory)
        to_return = copy.deepcopy(serializer.data)
        return Response(to_return, status=status.HTTP_201_CREATED)
    return Response(Laboratory.objects.none())

@api_view(['GET'])
def get_laboratory(request):
    if request.user.is_authenticated:
        laboratory = Laboratory.objects.filter(referent=request.user)
        if laboratory.exists():
            serializer = LaboratorySerializer(laboratory[0])
            to_return = copy.deepcopy(serializer.data)
            return Response(to_return)
        else :
            return Response(None)
    return Response(None, status=status.HTTP_400_BAD_REQUEST)
