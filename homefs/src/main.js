import Vue from 'vue'
import App from '@/App.vue'
import pinia from '@/stores/initPinia.js'
import router from '@/router'

import '@/utils/filters.js'
import '@/plugins/veevalidate.js'
import '@/utils/veerules.js'
import '@/plugins/buefy.js'
import { i18n } from '@/plugins/i18n.js'
import FlagIcon from 'vue-flag-icon'
import NProgress from 'vue-nprogress'
import VueClipboard from 'vue-clipboard2'
import VuePapaParse from 'vue-papa-parse'

Vue.use(FlagIcon)
Vue.use(VueClipboard)
Vue.use(VuePapaParse)

Vue.use(NProgress)
const nprogress = new NProgress()

Vue.config.productionTip = false

// Handle window scrolling for navbar and backtotop
let pxShow = 400
// let scrollSpeed = 500
window.addEventListener('scroll', function () {
  if (window.scrollY > 75) {
    let elements = document.getElementsByClassName('navbar')
    if (elements.length === 1) {
      elements[0].classList.add('navbar-fixed')
    }
  } else {
    let elements = document.getElementsByClassName('navbar')
    if (elements.length === 1) {
      elements[0].classList.remove('navbar-fixed')
    }
  }
  if (window.scrollY >= pxShow) {
    document.getElementById('backtotop').classList.add('visible')
  } else {
    document.getElementById('backtotop').classList.remove('visible')
  }
})

new Vue({
  nprogress,
  router,
  i18n,
  pinia,
  render: h => h(App)
}).$mount('#app')
