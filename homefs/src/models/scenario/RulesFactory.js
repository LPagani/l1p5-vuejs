/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import ComputerDevice from '@/models/carbon/ComputerDevice.js'
import FleetElectrification from '@/models/scenario/rules/FleetElectrification.js'
import AnnualCollectiveQuota from '@/models/scenario/rules/AnnualCollectiveQuota.js'
import CarElectrification from '@/models/scenario/rules/CarElectrification.js'
import Carpooling from '@/models/scenario/rules/Carpooling.js'
import ChangeHeating from '@/models/scenario/rules/ChangeHeating.js'
import IncreaseDevicesLifetime from '@/models/scenario/rules/IncreaseDevicesLifetime.js'
import IncreaseInstrumentsLifetime from '@/models/scenario/rules/IncreaseInstrumentsLifetime.js'
import InsulateBuilding from '@/models/scenario/rules/InsulateBuilding.js'
import LimitPlane from '@/models/scenario/rules/LimitPlane.js'
import ReduceHeating from '@/models/scenario/rules/ReduceHeating.js'
import ReducePurchase from '@/models/scenario/rules/ReducePurchase.js'
import ReplaceCar from '@/models/scenario/rules/ReplaceCar.js'
import ReplacePlane from '@/models/scenario/rules/ReplacePlane.js'
import ReplacePlaneNational from '@/models/scenario/rules/ReplacePlaneNational.js'
import SecondHandPurchase from '@/models/scenario/rules/SecondHandPurchase.js'
import SelfConsumption from '@/models/scenario/rules/SelfConsumption.js'
import Teleworking from '@/models/scenario/rules/Teleworking.js'
import ChangeDiet from '@/models/scenario/rules/ChangeDiet.js'

const classes = {
  ComputerDevice,
  FleetElectrification,
  AnnualCollectiveQuota,
  CarElectrification,
  Carpooling,
  ChangeHeating,
  IncreaseDevicesLifetime,
  IncreaseInstrumentsLifetime,
  InsulateBuilding,
  LimitPlane,
  ReduceHeating,
  ReducePurchase,
  ReplaceCar,
  ReplacePlane,
  ReplacePlaneNational,
  SecondHandPurchase,
  SelfConsumption,
  Teleworking,
  ChangeDiet
}

export function getBuiltRules () {
  return [
    new FleetElectrification(),
    new AnnualCollectiveQuota(),
    new CarElectrification(),
    new Carpooling(),
    new ChangeHeating(),
    new IncreaseDevicesLifetime(),
    new IncreaseInstrumentsLifetime(),
    new InsulateBuilding(),
    new LimitPlane(),
    new ReduceHeating(),
    new ReducePurchase(),
    new ReplaceCar(),
    new ReplacePlane(),
    new ReplacePlaneNational(),
    new SecondHandPurchase(),
    new SelfConsumption(),
    new Teleworking(),
    new ChangeDiet()
  ]
}

export function rulesFactory (name) {
  return classes[name]
}
