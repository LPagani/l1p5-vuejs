/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import Commute from '@/models/carbon/Commute.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Remplacer la voiture',
    null: 'Aucun',
    mode: 'Mode de transport alternatif',
    level1help: 'Remplacer un % de km parcourus en voiture par un mode de transport alternatif.',
    level2label: '(mode: <strong>__mode__</strong>)',
    level2help: 'Selectionner un mode de déplacement alternatif à la voiture.',
    description: 'D’après l’enquête mobilité des personnes 2018-2019 [1], les trajets domicile-travail aller ont une distance moyenne de 12,6 km pour une durée moyenne de 24,4 minutes. <strong>74% des trajets domicile-travail sont effectués en voiture</strong>, 12% en transports en commun, 9% à pied, 3% en vélo et 2% en 2 roues motorisés. <br /> Même pour des déplacements courts, la voiture reste largement majoritaire. Elle représente 60% des déplacements domicile-travail de moins de 5 km  et même 49% des déplacements domicile-travail de moins de 1km [2] alors que ce <strong>mode de déplacement</strong> est <strong>le plus émetteur</strong> de GES <strong>par personne et par km</strong> [3].',
    otherbenefits: 'L’utilisation des modes doux comme le vélo ou la marche permet de pratiquer quotidiennement un exercice physique.',
    limits: 'Même si le report modal de la voiture vers les modes doux peut être important au centre des aires urbaines [4], celui-ci reste plus difficile à mettre en place lorsque l’offre en transport en commun est réduite voir inexistante.',
    manual: 'Une fraction des distances parcourues en voiture est remplacée par un autre moyen de transport. Le curseur permet de spécifier le pourcentage de remplacement. Par défaut, le moyen de transport de remplacement est le vélo mécanique. La configuration avancée permet de spécifier un autre moyen de transport. Si l’on veut spécifier un remplacement par différents moyens, il faut créer autant de mesures que de moyens de substitution. L’outil vérifie que la somme totale des fractions substituées ne dépasse pas 100%. Ainsi, si on a déjà remplacé 50% des déplacements faits en voiture par un ou plusieurs autres moyens de transport, il ne sera pas possible d’allouer plus de 50% des déplacement à un nouveau moyen de transport. Il convient de bien vérifier que ces mesures ne rentrent pas en contradiction avec d’autres mesures, telles que le covoiturage. L’outil ne fait pas ces vérifications automatiquement.',
    humanTitle: function (level1Human, level2Human) {
      return `Remplacer ${level1Human} % des km parcourus en voiture par ${level2Human}`
    }
  },
  en: {
    title: 'Replace car',
    null: 'None',
    mode: 'Alternative mode of transportation',
    level1help: 'Replace a % of km traveled by car with an alternative mode of transportation.',
    level2label: '(mode: <strong>__mode__</strong>)',
    level2help: 'Select an alternative mode of transportation to the car.',
    description: 'According to the 2018-2019 personal mobility survey [1], one-way commuting trips have an average distance of 12.6 km and an average duration of 24.4 minutes. <strong>74% of commuting trips are made by car</strong>, 12% by public transportation, 9% by walking, 3% by bicycle, and 2% by motorized two-wheelers. <br /> Even for short distances, cars remain the predominant mode of transportation. They account for 60% of commuting trips under 5 km and even 49% of commuting trips under 1 km [2], despite being the <strong>highest emitter</strong> of greenhouse gases <strong>per person per kilometer</strong> [3].',
    otherbenefits: 'Using active modes of transportation such as cycling or walking allows for daily physical exercise.',
    limits: 'Although modal shift from cars to active modes of transportation can be significant in urban areas [4], it is more challenging to implement when public transportation options are limited or nonexistent.',
    manual: 'A fraction of the distances traveled by car is replaced by another mode of transportation. The slider allows you to specify the percentage of replacement. By default, the replacement mode of transportation is a regular bicycle. The advanced configuration allows you to specify another mode of transportation. If you want to specify replacements by different modes, you need to create a separate measure for each substitution mode. The tool verifies that the total sum of the substituted fractions does not exceed 100%. Therefore, if you have already replaced 50% of car trips with one or more other modes of transportation, it will not be possible to allocate more than 50% of the trips to a new mode of transportation. Make sure that these measures do not contradict other measures, such as carpooling. The tool does not perform these verifications automatically.',
    humanTitle: function (level1Human, level2Human) {
      return `Replace ${level1Human} % of km traveled by car by ${level2Human}`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Résultats détaillés de l’enquête mobilité des personnes de 2019',
    year: '2021',
    authors: 'Ministère de la Transition écologique et de la Cohésion des territoires',
    link: 'https://www.statistiques.developpement-durable.gouv.fr/comment-les-francais-se-deplacent-ils-en-2019-resultats-de-lenquete-mobilite-des-personnes'
  },
  '[2]': {
    title: 'La voiture reste majoritaire pour les déplacements domicile-travail, même pour de courtes distances',
    year: '2021',
    authors: 'Institut national de la statistique et des études économiques',
    link: 'https://www.insee.fr/fr/statistiques/5013868'
  },
  '[3]': {
    title: 'Mon Impact Transport',
    year: '2022',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://datagir.ademe.fr/apps/mon-impact-transport/'
  },
  '[4]': {
    title: 'Au centre des aires urbaines, un potentiel de report modal de la voiture vers le vélo de près de 45%',
    year: '2023',
    authors: 'Ministère de la Transition écologique et de la Cohésion des territoires',
    link: 'https://www.observatoire-des-territoires.gouv.fr/kiosque/2019-mobilite-14-au-centre-des-aires-urbaines-un-potentiel-de-report-modal-de-la-voiture'
  }
}

export default class ReplaceCar extends Rule {
  name = 'ReplaceCar'
  module = Modules.COMMUTES
  lang = LANG
  references = REFERENCES

  level2 = {
    mode: {
      values: function (commutes, otherrules = null) {
        let modes = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.mode.value)
          }
        }
        for (let mode of Commute.TRAVELS_MODES.filter(val => val !== 'car')) {
          if (!modes.map(obj => obj.id).includes(mode)) {
            modes.push({
              id: mode,
              label: mode,
              isDisabled: used.includes(mode)
            })
          }
        }
        return modes
      },
      unique: true,
      value: ['bike']
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get level1MaxValue () {
    let otherrules = this.scenario.rules.filter(obj => obj.name === this.name && obj.id !== this.id)
    return 100 - otherrules.map(obj => obj.level1).reduce((a, b) => a + b, 0)
  }

  get graphics () {
    return [
      'CommutesDistancesModes',
      'TransportsEmissionFactors'
    ]
  }

  compute (commutes) {
    for (let commute of commutes) {
      let rcommute = this.data.filter(obj => obj.seqID === commute.seqID)[0]
      for (let section of commute.sections) {
        if (section.mode === 'car' && section.distance > 0 && this.level2.mode.value !== null) {
          let rsection = rcommute.sections.filter(obj => obj.mode === 'car' && obj.distance > 0)[0]
          let newDistance = parseInt(rsection.distance * this.level1 / 100)
          section.distance -= newDistance
          commute.addSection({
            mode: this.level2.mode.value[0],
            distance: newDistance,
            isDay2: section.isDay2,
            pooling: 1,
            engine: null
          })
        }
      }
    }
    return commutes
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let commute of this.data) {
      const nbWeeks = commute.getNumberOfWorkedWeeks(settings, this.year)
      if (!commute.deleted) {
        for (let section of commute.sections) {
          if (section.mode === 'car' && section.distance > 0) {
            let annuelFactor = commute.nWorkingDay1 * nbWeeks
            if (section.isDay2) {
              annuelFactor = commute.nWorkingDay2 * nbWeeks
            }
            intensity.add(
              section.getCarbonIntensity(this.laboratory.citySize, this.year).multiply(annuelFactor)
            )
          }
        }
      }
    }
    return intensity
  }

  humanTitle (lang) {
    let level2Human = this.level2.code.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(this.code, level2Human)
  }
}
