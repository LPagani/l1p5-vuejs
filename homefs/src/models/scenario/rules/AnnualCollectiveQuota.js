/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Travel from '@/models/carbon/Travel.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Quota collectif annuel',
    level1help: 'Déterminer une distance maximale totale parcourue en avion par tous les personnels du laboratoires pour une année.',
    description: 'L’aviation représente <strong>4.3 % de l’empreinte carbone en France</strong> [1] et 5,1 % au niveau mondial [2]. A l’échelle individuelle, un vol aller-retour Paris / New York représente 17 % des émissions moyennes annuelles d’un·e français·e (1,7 tCO2e contre 9.9 tCO2e). Ce mode de transport est, avec la voiture, le mode le plus émetteur de GES. <br /> L’aviation est un mode de transport qui reste très inégalitaire. seuls <strong>4% des français prennent l’avion</strong> de façon régulière [3].',
    limits: 'Cette mesure demande aux personnels du laboratoire de négocier collectivement les modalités de partage de la distance totale autorisée. Ceci peut être source de tensions.',
    rebounds: 'La réduction des trajets réalisés en avion entraîne une réduction des dépenses de transport. Les <strong>marges budgétaires</strong> ainsi dégagées peuvent être <strong>ré-investies dans d’autres dépenses</strong> qui n’ont en général pas une empreinte carbone nulle. <br /> Ceci peut également entraîner un report vers des vols de plus courte distance qui ont des facteurs d’émission plus élevés que les trajets long-courriers (70% supérieur par passager et kilomètre pour les vols court-courriers) [4]',
    manual: 'Le curseur permet de spécifier la distance annuelle maximale totale parcourue en avion par tous les personnels du laboratoires. La borne supérieure correspond à la somme des déplacements pour lesquels un trajet a été réalisé en avion. Cette mesure ne propose pas de configuration avancée.',
    humanTitle: function (level1Human, level1Unit) {
      return `Mise en place d'un quota collectif de ${level1Human} ${level1Unit}`
    }
  },
  en: {
    title: 'Annual collective quota',
    level1help: 'Determine a maximum total distance traveled by air by all laboratory personnel for one year.',
    description: 'Aviation accounts for <strong>4.3% of carbon footprint in France</strong> [1] and 5.1% globally [2]. On an individual level, a round-trip flight from Paris to New York represents 17% of the average annual emissions of a French person (1.7 tCO2e compared to 9.9 tCO2e). This mode of transport is, along with cars, the most emitter of greenhouse gases. <br /> Aviation is a mode of transportation that remains highly unequal: only <strong>4% of French people regularly take flights</strong> [3].',
    limits: 'This measure requires laboratory personnel to negotiate collectively the terms of sharing the total authorised distance. This can be a source of tension.',
    rebounds: 'Reducing air travel leads to a reduction in transportation expenses. The <strong>budgetary margins</strong> thus released can be <strong>reinvested in other expenses</strong> that generally do not have a zero carbon footprint. <br /> This can also lead to a shift towards shorter flights which have higher emission factors than long-haul flights (70% higher per passenger and kilometer for short-haul flights) [4]',
    manual: 'The slider allows you to specify the maximum total annual distance traveled by air by all laboratory personnel. The upper limit corresponds to the sum of the trips for which a flight was taken. This measure does not offer advanced configuration.',
    humanTitle: function (level1Human, level1Unit) {
      return `Introuce annual collective quota of  ${level1Human} ${level1Unit}`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Empreinte carbone française moyenne, comment est-elle calculée ?',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo'
  },
  '[2]': {
    title: 'Les idées reçues sur l\'aviation et le climat',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/analyse-faq-aviation-climat'
  },
  '[3]': {
    title: 'Global inequalities in flying',
    year: '2021',
    authors: 'Possible.',
    link: 'https://static1.squarespace.com/static/5d30896202a18c0001b49180/t/605a0951f9b7543b55bb003b/1616513362894/Elite+Status+Global+inequalities+in+flying.pdf'
  },
  '[4]': {
    title: 'Court, Moyen, Long courrier (Valeurs moyennes)',
    year: '2018',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element/categorie/547'
  }
}

export default class AnnualCollectiveQuota extends Rule {
  name = 'AnnualCollectiveQuota'
  module = Modules.TRAVELS
  lang = LANG
  references = REFERENCES

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get level1Max () {
    let cumDistance = 0
    for (let travel of this.data) {
      if (travel.hasSectionWithPlane()) {
        cumDistance += travel.getDistance()
      }
    }
    return Math.ceil(cumDistance / 1000) * 1000
  }

  get level1Step () {
    return 1000
  }

  get level1Unit () {
    return ' km'
  }

  get ticks () {
    let cumDistance = 0
    for (let travel of this.data) {
      if (travel.hasSectionWithPlane()) {
        cumDistance += travel.getDistance()
      }
    }
    return [
      0,
      Math.ceil(cumDistance * 0.25 / 1000) * 1000,
      Math.ceil(cumDistance * 0.5 / 1000) * 1000,
      Math.ceil(cumDistance * 0.75 / 1000) * 1000,
      Math.ceil(cumDistance / 1000) * 1000
    ]
  }

  initiateLevel1 () {
    if (!this.level1 && this.level1 !== 0) {
      this.level1 = this.level1Max
    }
  }

  tickFormatter (value) {
    return value
  }

  compute (travels) {
    let filteredTravels = []
    let cumDistance = 0
    for (let travel of travels) {
      if (!travel.hasSectionWithPlane()) {
        filteredTravels.push(travel)
      } else if (cumDistance + travel.getDistance() <= this.level1) {
        filteredTravels.push(travel)
        cumDistance += travel.getDistance()
      }
    }
    return filteredTravels
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let travel of this.data) {
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE) {
          intensity.add(section.getCarbonIntensity(this.year).multiply(travel.amount))
        }
      }
    }
    return intensity
  }
}
