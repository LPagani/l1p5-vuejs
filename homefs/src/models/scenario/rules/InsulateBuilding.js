/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Building from '@/models/carbon/Building.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Isoler les bâtiments',
    all: 'Tous',
    building: 'Nom du bâtiment',
    level1help: 'Modifier la classe énergétique des bâtiments occupés par le laboratoire.',
    level2label: '(bâtiment: <strong>__building__</strong>)',
    level2help: 'Selectionner le bâtiment sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'Les <strong>travaux d’isolation</strong> des bâtiments permettent d’<strong>améliorer leur performance énergétique</strong> et donc de réduire leur consommation d’énergie pour le chauffage. En France, le diagnostic de performance énergétique [1] classe les bâtiments selon <strong>sept classes</strong> allant de A (pour la meilleure performance) à G (pour la pire). <br /> Ces classes et les consommations énergétiques associées sont illustrées sur la figure ci-jointe. Les émissions associées dépendent de la performance énergétique et des moyens de chauffage utilisés.',
    otherbenefits: 'Une meilleure isolation réduit également les besoins de climatisation éventuelle et améliore le confort thermique.',
    limits: 'L\'impact de cette mesure ne peut pas être estimée pour le chauffage électrique car la consommation énergétique est intégrée à la consommation électrique totale du laboratoire.',
    rebounds: 'Des effets rebonds peuvent être envisagés sur une telle mesure, telle que l’<strong>augmentation de la température de chauffage</strong> (par exemple, de 20°C à 22°C). <br /> Par ailleurs, la baisse de la consommation énergétique entraîne de moindres dépenses dans ce poste et une réutilisation possible des crédits ainsi dégagés dans d’autres postes émetteurs de GES [2] [3].',
    manual: 'La fenêtre d’information détaillée de la mesure indique la classe énergétique actuelle de votre ou de vos bâtiments. Elle donne également des informations détaillées par bâtiment si votre laboratoire en occupe plusieurs. Le curseur est positionné par défaut sur la classe moyenne actuelle du ou des bâtiments de votre laboratoire. Vous pouvez l’améliorer en déplaçant le curseur. En utilisant la configuration avancée, vous pouvez sélectionner un bâtiment précis sur lequel la mesure va s’appliquer. Par défaut, tous les bâtiments sont concernés. Si vous voulez appliquer la mesure indépendamment sur chaque bâtiment, il faut ajouter cette mesure autant de fois que de bâtiments concernés.',
    humanTitle: function (level1Human, level2Human) {
      return `Isoler les bâtiments ${level2Human} et atteindre la classe énergie ${level1Human}`
    }

  },
  en: {
    title: 'Insulate buildings',
    all: 'All',
    building: 'Building name',
    level1help: 'Modify the energy ratings of the buildings in which the laboratory is hosted.',
    level2label: '(building: <strong>__building__</strong>)',
    level2help: 'Select a building on which to filter the application of the mitigation measure.',
    description: 'Improving the <strong>insulation work</strong> of buildings enhances their <strong>energy performance</strong> and consequently reduces their energy consumption for heating. In France, the Energy Performance Certificate [1] categorizes buildings into <strong>seven classes</strong>, ranging from A (for the best performance) to G (for the worst). <br /> These classes and their associated energy consumption are illustrated in the attached figure. The associated emissions depend on the energy performance and heating methods used.',
    otherbenefits: 'Better insulation also reduces the need for potential air conditioning and improves thermal comfort.',
    limits: 'The impact of this measure cannot be estimated for electric heating as the energy consumption is included in the total electricity consumption of the laboratory.',
    rebounds: 'Rebound effects can be expected from such a measure, such as an <strong>increase in heating temperature</strong> (e.g., from 20°C to 22°C). <br /> Moreover, the decrease in energy consumption leads to reduced expenses, allowing for possible allocation of the freed-up credits to other GHG emitting purposes [2] [3].',
    manual: 'The detailed information window of the measure indicates the current energy class of your building(s). It also provides detailed information per building if your laboratory occupies multiple buildings. By default, the slider is positioned on the current average class of your laboratory building(s). You can improve it by adjusting the slider. By using the advanced configuration, you can select a specific building to which the measure will apply. By default, all buildings are affected. If you want to apply the measure independently to each building, you need to add this measure as many times as there are relevant buildings.'
  },

  humanTitle: function (level1Human, level2Human) {
    return `Insulate the buildings ${level2Human} and reach the energy rating ${level1Human}`
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Diagnostic de performance énergétique',
    year: '2023',
    authors: 'Wikipedia',
    link: 'https://fr.wikipedia.org/wiki/Diagnostic_de_performance_%C3%A9nerg%C3%A9tique'
  },
  '[2]': {
    title: 'L’effet rebond ou l’illustration de l’impact du comportement sur la transition énergétique',
    year: '2021',
    authors: 'egreen',
    link: 'https://www.egreen.fr/post/l-effet-rebond-ou-l-illustration-de-l-impact-du-comportement-sur-la-transition-energetique'
  },
  '[3]': {
    title: 'En Allemagne, les rénovations énergétiques des bâtiments n’ont pas fait baisser la consommation',
    year: '2020',
    authors: 'Le Monde',
    link: 'https://www.lemonde.fr/economie/article/2020/10/04/en-allemagne-les-renovations-energetiques-des-batiments-n-ont-pas-fait-baisser-la-consommation_6054715_3234.html'
  }
}

export default class InsulateBuilding extends Rule {
  name = 'InsulateBuilding'
  module = Modules.HEATINGS
  lang = LANG
  references = REFERENCES

  level2 = {
    building: {
      values: function (buildings, otherrules = null) {
        let names = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.building.value)
          }
        }
        for (let building of buildings) {
          if (!names.map(obj => obj.id).includes(building.name)) {
            names.push({
              id: building.name,
              label: building.name,
              isDisabled: used.includes(building.name)
            })
          }
        }
        return names
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get level1Max () {
    return 6
  }

  get level1Unit () {
    return ''
  }

  get level1Ticks () {
    let ticks = []
    for (let i in Building.ENERGETIC_CLASSES) {
      ticks.push({
        value: parseInt(i),
        display: Building.ENERGETIC_CLASSES[i]
      })
    }
    return ticks
  }

  get level1MaxValue () {
    let values = Building.getEnergeticValues(this.data)
    return Building.ENERGETIC_CLASSES.indexOf(values[0])
  }

  labelFormatter (value) {
    if (value === 0) return 'A (≤ 70 kWh/m2.an)'
    else if (value === 1) return 'B (71-110 kWh/m2.an)'
    else if (value === 2) return 'C (111-180 kWh/m2.an)'
    else if (value === 3) return 'D (181-250 kWh/m2.an)'
    else if (value === 4) return 'E (251-330 kWh/m2.an)'
    else if (value === 5) return 'F (331-420 kWh/m2.an)'
    else if (value === 6) return 'G (> 420 kWh/m2.an)'
  }

  get graphics () {
    return [
      'EnergyPerformance'
    ]
  }

  initiateLevel1 () {
    if (!this.level1 && this.data) {
      let values = Building.getEnergeticValues(this.data)
      this.level1 = Building.ENERGETIC_CLASSES.indexOf(values[0])
    }
  }

  compute (buildings) {
    let newConsumption = [35, 90, 145, 215, 290, 375, 500]
    let buildingsToModify = buildings
    if (this.level2.building.value.length > 0) {
      buildingsToModify = buildings.filter(obj => this.level2.building.value.includes(obj.name))
    }
    for (let building of buildingsToModify) {
      let values = building.getEnergeticValues(this.year)
      let vindex = Building.ENERGETIC_CLASSES.indexOf(values[0])
      // change consumption only if better class
      if (vindex > this.level1) {
        for (let heating of building.heatings) {
          if (heating.type !== 'electric') {
            if (heating.isMonthly) {
              let newMonthlyConsumption = newConsumption[this.level1] * building.area / 12
              heating.january = newMonthlyConsumption
              heating.february = newMonthlyConsumption
              heating.march = newMonthlyConsumption
              heating.april = newMonthlyConsumption
              heating.may = newMonthlyConsumption
              heating.june = newMonthlyConsumption
              heating.july = newMonthlyConsumption
              heating.august = newMonthlyConsumption
              heating.septembre = newMonthlyConsumption
              heating.octobre = newMonthlyConsumption
              heating.novembre = newMonthlyConsumption
              heating.decembre = newMonthlyConsumption
            } else {
              heating.total = newConsumption[this.level1] * building.area
            }
          }
        }
      }
    }
    return buildings
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let building of this.data) {
      for (let heating of building.heatings) {
        intensity.add(building.getHeatingCarbonIntensity(heating, this.year))
      }
    }
    return intensity
  }

  humanTitle (lang) {
    let level1Human = this.labelFormatter(this.level1)
    let level2Human = this.level2.building.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(level1Human, level2Human)
  }
}
