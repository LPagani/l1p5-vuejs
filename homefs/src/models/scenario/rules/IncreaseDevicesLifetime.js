/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Modules from '@/models/Modules.js'
import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Durabilité des mat. info.',
    all: 'Tous',
    devicetypes: 'Matériel informatique',
    level1help: 'Augmenter la durée de vie des matériels informatiques achetés par le laboratoire.',
    level2label: '(matériel: <strong>__devicetypes__</strong>)',
    level2help: 'Selectionner un type de matériel sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'Le numérique représente <strong>2.5% de l\'empreinte carbone en France</strong> et a une croissance annuelle estimée à 6 % [1]. <br /> La <strong>fabrication</strong> des matériels informatiques <strong>représente la majorité de leur empreinte carbone en France</strong>, loin devant leur usage [1]. Augmenter leur durée de vie permet ainsi de réduire leur empreinte carbone.',
    otherbenefits: 'Cette mesure peut contribuer à <strong>diminuer la quantité de déchets</strong> produits.',
    limits: 'Augmenter la durée de vie peut rendre les matériels moins performants et réduire la qualité de vie au travail des personnels. <br /> Cette mesure <strong>ne prend pas en compte les émissions associées à la maintenance et à la réparation</strong>.',
    rebounds: 'Ne pas renouveler du matériel informatique peut <strong>dégager de nouveaux crédits</strong> qui peuvent être <strong>engagés pour de nouvelles dépenses</strong> potentiellement <strong>plus émettrices</strong> en terme de GES. Dans tous les cas, si les crédits non dépensés sont utilisés pour d’autres usages, cela induit une émission nouvelle de GES qui vient diminuer l’efficacité de la mesure.',
    manual: ' Le curseur permet de spécifier l’augmentation relative de la durée de vie du matériel informatique. Le maximum proposé est de 100% ce qui correspond à un doublement de cette durée de vie. Par défaut, tous les matériels informatiques sont concernés par cette mesure. En accédant à la configuration avancée, il est possible de définir plus finement le matériel concerné. La sélection multiple des matériels concernés est possible. Cette mesure peut être ajoutée plusieurs fois si elle n’est appliquée que sur une partie du matériel informatique. En revanche, les catégories de matériel déjà choisies ne peuvent pas être re-sélectionnées.',
    humanTitle: function (level1Human, level2Human) {
      let s = `Augmenter de ${level1Human} % la durée de vie`
      if (level2Human) {
        return s + ` des ${level2Human}`
      } else {
        return s + ` de tous les équipements`
      }
    }
  },
  en: {
    title: 'Durability of comp. eq.',
    all: 'All',
    devicetypes: 'Computer device',
    level1help: 'Increase the lifespan of computer devices purchased by the laboratory.',
    level2label: '(equipment: <strong>__devicetypes__</strong>)',
    level2help: 'Select a type of computer device on which to filter the application of the mitigation measure.',
    description: 'Digital represents <strong>2.5% of the carbon footprint in France</strong> and has an estimated annual growth rate of 6% [1]. The <strong>manufacture</strong> of IT equipment <strong>represents the majority of its carbon footprint in France</strong>, far ahead of its use. Increasing their lifespan thus reduces their carbon footprint.',
    otherbenefits: 'This measure can help to <strong>decrease the amount of waste</strong> produced.',
    limits: 'Increasing the lifespan can make equipment less performing and reduce the quality of work life for staff. <br /> This measure <strong>does not take into account emissions associated with additional maintenance and repair</strong>.',
    rebounds: 'Not renewing IT equipment can <strong>free up new credits</strong> that can be spent on new, potentially <strong>more GHG emitting</strong> expenditure. In any case, if the unspent credits are allocated to other purposes, this leads to a new GHG emission which reduces the effectiveness of the measure.',
    manual: 'The slider allows you to specify the relative increase in the lifetime of the computer devices. The maximum proposed is 100%, which corresponds to a doubling of the lifetime. By default, all devices are affected by this measure. By accessing the advanced configuration, it is possible to define more precisely which computer devices are concerned. Multiple selection of affected devices is possible. This measure can be added several times if it is only applied to part of the hardware. However, hardware categories already selected cannot be re-selected.',
    humanTitle: function (level1Human, level2Human) {
      let s = `Increase the lifespan of ${level1Human} %`
      if (level2Human) {
        return s + ` for the ${level2Human}`
      } else {
        return s + `of all computer devices`
      }
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'L\'empreinte environnementale du numérique',
    year: '2023',
    authors: 'ADEME, Arcep',
    link: ' https://www.arcep.fr/la-regulation/grands-dossiers-thematiques-transverses/lempreinte-environnementale-du-numerique.html'
  },
  '[2]': {
    title: 'Pourquoi et comment faire durer son ordinateur ?',
    year: '2018',
    authors: 'Association négaWatt',
    link: 'https://negawatt.org/IMG/pdf/181112_faire-durer-son-ordinateur.pdf'
  }
}

export default class IncreaseDevicesLifetime extends Rule {
  name = 'IncreaseDevicesLifetime'
  module = Modules.DEVICES
  target = Rule.TARGET_EMISSIONS
  lang = LANG
  references = REFERENCES

  level2 = {
    devicetypes: {
      values: function (devices, otherrules = null) {
        let types = []
        let used = []
        if (otherrules) {
          for (let param of otherrules) {
            used = used.concat(param.devicetypes.value)
          }
        }
        for (let device of devices) {
          if (!types.map(obj => obj.id).includes(device.type)) {
            types.push({
              id: device.type,
              label: device.type,
              isDisabled: used.includes(device.type)
            })
          }
        }
        return types
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get graphics () {
    return [
      'DevicesPie'
    ]
  }

  compute (intensity, levels1, levels2, devices) {
    let nDevicesToModify = 0
    let intensities = new CarbonIntensities()
    for (let device of devices) {
      let toModify = false
      let cintensity = device.getCarbonIntensity()
      for (let lindex in levels2) {
        if (levels2[lindex].devicetypes.value.includes(device.type)) {
          intensities.add(new CarbonIntensity(
            cintensity.intensity * (100 / (100 + levels1[lindex])),
            cintensity.uncertainty * (100 / (100 + levels1[lindex]))
          ))
          nDevicesToModify += 1
          toModify = true
        }
      }
      if (!toModify) {
        intensities.add(new CarbonIntensity(
          cintensity.intensity,
          cintensity.uncertainty
        ))
      }
    }
    let newintensity = intensities.sum()
    if (nDevicesToModify === 0) {
      newintensity = new CarbonIntensity(
        intensity.intensity * (100 / (100 + levels1[0])),
        intensity.uncertainty * (100 / (100 + levels1[0]))
      )
    }
    return newintensity
  }

  humanTitle (lang) {
    let level2Human = this.level2.devicetypes.value.join(', ')
    return Rule.translate(this, 'humanTitle', lang)(this.level1, level2Human)
  }
}
