/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import Rule from '../Rule.js'
import Travel from '@/models/carbon/Travel.js'
import Modules from '@/models/Modules.js'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

const LANG = {
  fr: {
    title: 'Limiter l\'avion',
    all: 'Tous',
    position: 'Statut de l\'agent',
    purpose: 'Motif de la mission',
    level1help: 'Limiter les déplacements en avion dépassant la distance fixée en kilomètres.',
    level2label: '(statut: <strong>__position__</strong>, motif: <strong>__purpose__</strong>)',
    level2help: 'Selectionner un statut et/ou un motif sur lequel filtrer l\'application de la mesure de réduction.',
    description: 'L’aviation représente <strong>4.3 % de l’empreinte carbone en France</strong> [1] et 5,1 % au niveau mondial [2]. A l’échelle individuelle, un vol aller-retour Paris / New York représente 17 % des émissions moyennes annuelles d’un·e français·e (1,7 tCO2e contre 9.9 tCO2e). Ce mode de transport est, avec la voiture, le mode le plus émetteur de GES. <br /> L’aviation est un mode de transport qui reste très inégalitaire. seuls <strong>4% des français prennent l’avion</strong> de façon régulière [3].',
    limits: 'Cette mesure peut poser des problèmes importants dans certaines unités, par exemple celles travaillant sur des chantiers lointains ou ayant des collaborations essentielles avec des pays très éloignés.',
    rebounds: 'La réduction des trajets réalisés en avion entraîne une réduction des dépenses de transport. Les <strong>marges budgétaires</strong> ainsi dégagées peuvent être <strong>ré-investies dans d’autres dépenses</strong> qui n’ont en général pas une empreinte carbone nulle. <br /> Ceci peut également entraîner un report vers des vols de plus courte distance qui ont des facteurs d’émission plus élevés que les trajets long-courriers (70% supérieur par passager et kilomètre pour les vols court-courriers) [4]',
    manual: 'Le curseur permet de spécifier une distance maximale autorisée pour un trajet en avion. La configuration avancée permet de sélectionner le type de personnel et/ou le motif de la mission concernés par la mesure. Par défaut cette mesure s’applique à tous les trajets. Cette configuration avancée n’est proposée que si votre laboratoire a fourni ces informations (motifs de déplacement et statuts professionnels des missionnaires).',
    humanTitle: function (level1Human, level1Unit) {
      // FIXME(msimonin): deals with levels 2
      return `Limiter les déplacements en avion excédant ${level1Human} ${level1Unit}`
    }
  },
  en: {
    title: 'Limiting air travel',
    all: 'All',
    position: 'Position',
    purpose: 'Travel purpose',
    level1help: 'Limit air travel exceeding the specified distance in kilometers.',
    level2label: '(status: <strong>__position__</strong>, purpose: <strong>__purpose__</strong>)',
    level2help: 'Select a status and/or purpose to filter the application of the mitigation measure.',
    description: 'Aviation accounts for <strong>4.3% of carbon footprint in France</strong> [1] and 5.1% globally [2]. On an individual level, a round-trip flight from Paris to New York represents 17% of the average annual emissions of a French person (1.7 tCO2e compared to 9.9 tCO2e). This mode of transport is, along with cars, the most emitter of greenhouse gases. <br /> Aviation is a mode of transport that remains highly unequal: only <strong>4% of French people regularly take flights</strong> [3].',
    limits: 'This measure can pose significant challenges in certain units, for example, those working on remote construction sites or having essential collaborations with geographically distant countries.',
    rebounds: 'Reducing air travel leads to reduced transportation expenses. The resulting <strong>budgetary margins</strong> can be <strong>reinvested in other expenses</strong>, which generally have a non-zero carbon footprint. <br /> This can also lead to a shift towards shorter flights that have higher emission factors than long-haul trips (70% higher per passenger-kilometer for short-haul flights) [4].',
    manual: 'The slider allows you to specify the maximum distance permitted for an air travel journey. The advanced configuration allows you to select the type of personnel and/or mission purpose affected by the measure. By default, this measure applies to all trips. The advanced configuration is only provided if your laboratory has provided this information (travel purposes and professional statuses of mission participants).',
    humanTitle: function (level1Human, level1Unit) {
      // FIXME(msimonin): deals with levels 2
      return `Limit air travel exceeding ${level1Human} ${level1Unit}`
    }
  }
}

const REFERENCES = {
  '[1]': {
    title: 'Empreinte carbone française moyenne, comment est-elle calculée ?',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/myco2-empreinte-moyenne-evolution-methodo'
  },
  '[2]': {
    title: 'Les idées reçues sur l\'aviation et le climat',
    year: '2022',
    authors: 'Carbone4',
    link: 'https://www.carbone4.com/analyse-faq-aviation-climat'
  },
  '[3]': {
    title: 'Global inequalities in flying',
    year: '2021',
    authors: 'Possible.',
    link: 'https://static1.squarespace.com/static/5d30896202a18c0001b49180/t/605a0951f9b7543b55bb003b/1616513362894/Elite+Status+Global+inequalities+in+flying.pdf'
  },
  '[4]': {
    title: 'Court, Moyen, Long courrier (Valeurs moyennes)',
    year: '2018',
    authors: 'ADEME, Agence de la transition écologique',
    link: 'https://bilans-ges.ademe.fr/fr/basecarbone/donnees-consulter/liste-element/categorie/547'
  }
}

export default class LimitPlane extends Rule {
  name = 'LimitPlane'
  module = Modules.TRAVELS
  lang = LANG
  references = REFERENCES

  level2 = {
    position: {
      values: function (travels) {
        let positions = []
        for (let travel of travels) {
          if (!positions.map(obj => obj.id).includes(travel.status)) {
            positions.push({
              id: travel.status,
              label: travel.status
            })
          }
        }
        return positions
      },
      value: []
    },
    purpose: {
      values: function (travels) {
        let purposes = []
        for (let travel of travels) {
          if (!purposes.map(obj => obj.id).includes(travel.purpose)) {
            purposes.push({
              id: travel.purpose,
              label: travel.purpose
            })
          }
        }
        return purposes
      },
      value: []
    }
  }

  constructor (id, scenario = null, level1 = null, level2 = {}) {
    super(id, scenario, level1, level2)
    this.initiateLevel2(level2)
  }

  get ticks () {
    let maxDistance = 0
    for (let travel of this.data) {
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE) {
          if (section.distance > maxDistance) {
            maxDistance = section.distance
          }
        }
      }
    }
    return [
      0,
      Math.ceil(maxDistance * 0.25 / 100) * 100,
      Math.ceil(maxDistance * 0.5 / 100) * 100,
      Math.ceil(maxDistance * 0.75 / 100) * 100,
      Math.ceil(maxDistance / 100) * 100
    ]
  }

  get level1Step () {
    return 100
  }

  initiateLevel1 () {
    if (!this.level1 && this.data) {
      let maxDistance = 0
      for (let travel of this.data) {
        let cdistance = travel.getDistance()
        if (cdistance > maxDistance) {
          maxDistance = cdistance
        }
      }
      this.level1 = Math.ceil(maxDistance / 100) * 100
    }
  }

  get level1Max () {
    let maxDistance = 0
    for (let travel of this.data) {
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE) {
          if (section.distance > maxDistance) {
            maxDistance = section.distance
          }
        }
      }
    }
    return Math.ceil(maxDistance / 100) * 100
  }

  get level1Unit () {
    return 'km'
  }

  get graphics () {
    return [
      'PlanesCumulativeEmissions',
      'PlanesAndTrainsEmissionFactors'
    ]
  }

  tickFormatter (value) {
    return value
  }

  compute (travels) {
    let filteredTravels = []
    for (let travel of travels) {
      let hasLongSection = false
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE && section.distance > this.level1) {
          hasLongSection = true
        }
      }
      if (hasLongSection) {
        if (this.level2.position.value.length > 0 && this.level2.purpose.value.length > 0) {
          if (!this.level2.position.value.includes(travel.status) && !this.level2.purpose.value.includes(travel.purpose)) {
            filteredTravels.push(travel)
          }
        } else if (this.level2.position.value.length > 0) {
          if (!this.level2.position.value.includes(travel.status)) {
            filteredTravels.push(travel)
          }
        } else if (this.level2.purpose.value.length > 0) {
          if (!this.level2.purpose.value.includes(travel.purpose)) {
            filteredTravels.push(travel)
          }
        }
      } else {
        filteredTravels.push(travel)
      }
    }
    return filteredTravels
  }

  getTargetIntensity (settings) {
    let intensity = new CarbonIntensities()
    for (let travel of this.data) {
      for (let section of travel.sections) {
        if (section.transportation === Travel.MODE_PLANE) {
          intensity.add(section.getCarbonIntensity(this.year).multiply(travel.amount))
        }
      }
    }
    return intensity
  }
}
