import Vehicle from '@/models/carbon/Vehicle'
import FleetElectrification from './FleetElectrification'

/**
 *
 * @param {Object} modifier key, value to modify in the default rule Object
 *
 * @returns {Vehicle}
 */
function vehiclesObj () {
  let vehicle = {
    id: null,
    type: 'car',
    name: null,
    engine: 'diesel',
    consumption: {
      total: 69000,
      isMonthly: false
    },
    unit: 'km',
    power: null,
    noEngine: null,
    shp: null,
    controled: true
  }
  return [Vehicle.createFromObj(vehicle)]
}
function ruleObj (modifier) {
  let rule = new FleetElectrification(
    null,
    null,
    0
  )

  if (modifier !== undefined) {
    rule.level1 = modifier
  }

  return rule
}

describe('compute', () => {
  test('0% of electrification', () => {
    let vehicles = vehiclesObj()
    let rule = ruleObj()
    let nvehicles = rule.compute(
      vehicles
    )
    expect(nvehicles[0].engine).toBe(vehicles[0].engine)
  })
  test('100% of electrification', () => {
    let vehicles = vehiclesObj()
    let rule = ruleObj(100)
    let nvehicles = rule.compute(
      vehicles
    )
    expect(nvehicles[0].engine).toBe('electric')
  })
})
