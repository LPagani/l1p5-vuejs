import {
  PublicAction,
  makeTags,
  makeAdminStatuses,
  makeUserStatuses,
  makeUserStatus,
  Tag
} from './transition'

const ONE_DAY_MS = 1 * 24 * 3600 * 1000

describe('Action.isReviewerLate', () => {
  test('Recent modified action are not late but old are', () => {
    // recent action (until 5 days)
    for (let i of [1, 2, 3, 4, 5]) {
      let current = (new Date()).getTime()
      let action = new PublicAction().setUpdatedAt(current - i * ONE_DAY_MS)
      expect(action.isReviewLate()).toBeFalsy()
    }

    // recent action (until 5 days)
    for (let i of [6, 7, 8, 9, 10]) {
      let current = (new Date()).getTime()
      let action = new PublicAction().setUpdatedAt(current - i * ONE_DAY_MS)
      expect(action.isReviewLate()).toBeTruthy()
    }
  })
})

describe('Action.status', () => {
  test('Start and End in the past leads to a terminated action', () => {
    let now = new Date()
    const oneDay = 3600 * 1000 * 24
    let start = new Date(now.getTime() - 2 * oneDay)
    let end = new Date(now.getTime() - oneDay)

    let action = new PublicAction().setStart(start).setEnd(end)
    expect(action.status).toEqual(makeUserStatus('userstatus:terminated'))
  })

  test('Start in the past and end in the future lead to a running action', () => {
    let now = new Date()
    const oneDay = 3600 * 1000 * 24
    let start = new Date(now.getTime() - oneDay)
    let end = new Date(now.getTime() + oneDay)

    let action = new PublicAction().setStart(start).setEnd(end)
    expect(action.status).toEqual(makeUserStatus('userstatus:running'))
  })
})

describe('make*', () => {
  test('', () => {
    let tags = makeTags(['tag:ges:buildings', 'tag:ges:purchases'])
    expect(tags.length).toBe(2)
    // they are singleton
    let a = makeTags(['tag:ges:buildings'])
    let b = makeTags(['tag:ges:buildings'])
    expect(Object.is(a[0], b[0])).toBe(true)

    let adminStatuses = makeAdminStatuses([
      'adminstatus:draft',
      'adminstatus:submitted',
      'adminstatus:published'
    ])
    a = makeAdminStatuses(['adminstatus:published'])
    b = makeAdminStatuses(['adminstatus:published'])
    expect(adminStatuses.length).toBe(3)
    expect(Object.is(a[0], b[0])).toBe(true)

    let userStatuses = makeUserStatuses([
      'userstatus:running',
      'userstatus:paused',
      'userstatus:terminated',
      'userstatus:abandoned'
    ])
    expect(userStatuses.length).toBe(4)
    a = makeUserStatuses(['userstatus:running'])
    b = makeUserStatuses(['userstatus:running'])
    expect(Object.is(a[0], b[0])).toBe(true)
  })
})

describe('clone', () => {
  let action = new PublicAction()
    .setTitle('title test')
    .setText('text test')
    .setTags(makeTags(['tag:ges:buildings', 'tag:ges:travels']))
  let clonedAction = action.clone()
  expect(action.title).toBe(clonedAction.title)
  expect(action.text).toBe(clonedAction.text)
  // check that we conserve the singleton nature of each action tags
  expect(action.tags.length).toBe(clonedAction.tags.length)
  for (let i in action.tags) {
    // strict equality
    expect(action.tags[i]).toBe(clonedAction.tags[i])
  }
})

describe('Tag.match', () => {
  test('match fr', () => {
    let tag = Tag.fromJSON({
      descriptor: 'azerty-desc',
      i18n: {
        tag: {
          fr: 'azerty',
          en: 'uiop'
        }
      }
    })
    // translated as Bâtiments
    expect(tag.match('a')).toBeTruthy()
    expect(tag.match('az')).toBeTruthy()
    expect(tag.match('aze')).toBeTruthy()
    expect(tag.match('azerty')).toBeTruthy()
    expect(tag.match('ayertyu')).toBeFalsy()
    // some accents
    expect(tag.match('à')).toBeTruthy()
    expect(tag.match('âz')).toBeTruthy()
    expect(tag.match('azè')).toBeTruthy()
    // capitalize
    expect(tag.match('A')).toBeTruthy()
    expect(tag.match('Az')).toBeTruthy()
    expect(tag.match('AZE')).toBeTruthy()
    expect(tag.match('AzErTy')).toBeTruthy()

    // en
    expect(tag.match('u')).toBeTruthy()
    expect(tag.match('ui')).toBeTruthy()
    expect(tag.match('uiop')).toBeTruthy()
  })
})
