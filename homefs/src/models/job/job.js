export class Job {
  constructor () {
    this.id = null
    this.name = null
    // Date
    this.created = null
    // Date
    this.finished = null
    this.stdout = null
    this.stderr = null
    this.rc = null
    this.error = null
    this.uuid = null
  }

  setId (id) {
    this.id = id
    return this
  }

  setName (name) {
    this.name = name
    return this
  }

  setCreated (created) {
    this.created = created
    return this
  }

  setFinished (finished) {
    this.finished = finished
    return this
  }

  setStdout (stdout) {
    this.stdout = stdout
    return this
  }

  setStderr (stderr) {
    this.stderr = stderr
    return this
  }

  setRC (rc) {
    this.rc = rc
    return this
  }

  setError (error) {
    this.error = error
    return this
  }

  setUUID (uuid) {
    this.uuid = uuid
    return this
  }

  static createFromObj (data) {
    let job = new Job()
      .setId(data.id)
      .setName(data.name)
      .setCreated(new Date(data.created))
      .setStdout(data.stdout)
      .setStderr(data.stderr)
      .setRC(data.rc)
      .setError(data.error)
      .setUUID(data.uuid)

    if (data.finished) {
      job.setFinished(new Date(data.finished))
    }

    return job
  }
}
