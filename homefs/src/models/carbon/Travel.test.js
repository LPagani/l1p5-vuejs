import * as T from './Travel'
import Travel from './Travel'

function parisToRennes () {
  let section = {
    transportation: T.MODE_PLANE,
    departureCity: 'Paris',
    departureCountry: 'FR',
    destinationCity: 'Rennes',
    destinationCountry: 'FR',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    destinationCityLat: 48.11,
    destinationCityLng: -1.68,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['paris_rennes'],
    date: '2022-04-01',
    sections: [section],
    amount: 1,
    purpose: T.PURPOSE_OTHER,
    status: T.STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

function parisToNewYork () {
  let section = {
    transportation: T.MODE_PLANE,
    departureCity: 'Paris',
    departureCountry: 'FR',
    destinationCity: 'NEW YORK',
    destinationCountry: 'US',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    destinationCityLat: 40.68,
    destinationCityLng: -74.04,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['paris_newyork'],
    date: '2022-04-01',
    sections: [section],
    amount: 1,
    purpose: T.PURPOSE_OTHER,
    status: T.STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

function ParisToAuckland () {
  let section1 = {
    transportation: T.MODE_PLANE,
    departureCity: 'Paris',
    departureCountry: 'FR',
    destinationCity: 'ABU DABI',
    destinationCountry: 'UAE',
    carpooling: 1,
    distance: 0,
    departureCityLat: 48.8555,
    departureCityLng: 2.348,
    destinationCityLat: 24.3867414,
    destinationCityLng: 54.3938126,
    isRoundTrip: false
  }

  let section2 = {
    transportation: T.MODE_PLANE,
    departureCity: 'ABU DABI',
    departureCountry: 'UAE',
    destinationCity: 'AUCKLAND',
    destinationCountry: 'NZ',
    carpooling: 1,
    distance: 0,
    departureCityLat: 24.3867414,
    departureCityLng: 54.3938126,
    destinationCityLat: -36.8596971,
    destinationCityLng: 174.5413149,
    isRoundTrip: false
  }

  return Travel.createFromObj({
    names: ['paris_auckland'],
    date: '2022-04-01',
    sections: [section1, section2],
    amount: 1,
    purpose: T.PURPOSE_OTHER,
    status: T.STATUS_ENGINEER,
    // source seems unused
    source: null
  })
}

describe('Reduce travels', () => {
  test('Reduce two same travels into one', () => {
    let t1 = parisToRennes()
    let t2 = parisToRennes()
    let travels = Travel.reduce([t1, t2])

    expect(travels.length).toBe(1)
    // name must be a list of string
    expect(travels[0].names).toEqual(['paris_rennes', 'paris_rennes'])
  })

  test('Reduce two different travels', () => {
    let t1 = parisToRennes()
    let t2 = parisToNewYork()
    let travels = Travel.reduce([t1, t2])

    expect(travels.length).toBe(2)
    expect(travels[0].names).toEqual(['paris_rennes'])
    expect(travels[1].names).toEqual(['paris_newyork'])
  })

  test('Reduce with already reduced travels', () => {
    let t1 = parisToRennes()
    t1.amount = 3
    let t2 = parisToRennes()
    t2.amount = 4
    let travels = Travel.reduce([t1, t2])

    expect(travels.length).toBe(1)
    expect(travels[0].amount).toBe(7)
  })
})

describe('Compute plane distance', () => {
  test('Paris -> Rennes', () => {
    let travel = parisToRennes()
    // google earth distance + hardcoded corrected distance
    expect(travel.getDistance()).toBeGreaterThan(380)
    expect(travel.getDistance()).toBeLessThan(420)
  })

  test('Paris -> New York', () => {
    let travel = parisToNewYork()
    // google earth distance + hardcoded corrected distance
    expect(travel.getDistance()).toBeGreaterThan(5900)
    expect(travel.getDistance()).toBeLessThan(6100)
  })
})

describe('Export travels', () => {
  test('Paris -> Rennes', () => {
    let travel = parisToRennes()
    expect(travel.sections[0].toString()).toEqual(
      [
        'NA',
        'plane',
        '1',
        'false',
        '308.18227034329306',
        '403.18227034329306',
        '403.18227034329306',
        '0',
        '0'
      ].join('\t')
    )
  })

  test('Paris -> New York', () => {
    let travel = parisToNewYork()
    expect(travel.sections[0].toString()).toEqual(
      [
        'MX',
        'plane',
        '1',
        'false',
        '5841.189422053022',
        '5936.189422053022',
        '5936.189422053022',
        '0',
        '0'
      ].join('\t')
    )
  })

  test('Paris -> Auckland (2 sections)', () => {
    let travel = ParisToAuckland()
    // we have 2 lines ( one per unique section )
    expect(travel.toString().split('\n').length).toBe(2)
  })
})
