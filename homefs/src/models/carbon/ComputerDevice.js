
/* eslint-disable camelcase */

import { device_utils } from '@/ext/ecodiag/device_utils.js'
import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

export default class ComputerDevice {
  constructor (id, type, model, amount, acquisitionYear, ecodiagObject = undefined) {
    this.id = id
    this.type = type
    this.model = model
    this.amount = amount
    this.acquisitionYear = parseInt(acquisitionYear)
    this.ecodiagObject = ecodiagObject
    this.intensity = new CarbonIntensity()
  }

  toString (sep = '\t') {
    return [
      this.type,
      this.model,
      this.amount,
      Math.round(this.intensity.intensity),
      Math.round(this.intensity.uncertainty)
    ].join(sep)
  }

  toEcodiag () {
    if (!this.ecodiagObject) {
      this.ecodiagObject = device_utils.methods.create_device_item(this.type, {
        id: this.id,
        model: this.model,
        nb: this.amount,
        year: this.acquisitionYear,
        score: 2
      })
    }
    return this.ecodiagObject
  }

  toDatabase () {
    return {
      'type': this.type,
      'model': this.model,
      'amount': this.amount,
      'acquisitionYear': this.acquisitionYear
    }
  }

  static type2group = function (type) {
    try {
      return {
        'desktop': 'desktopgroup',
        'server': 'desktopgroup',
        'allinone': 'desktopgroup',
        'smartphone': 'smartphonegroup',
        'pad': 'smartphonegroup' }[type]
    } catch (e) {
      return type
    }
  }

  getCarbonIntensity () {
    let up = [90]
    let edintensity = device_utils.methods.compute_device_co2e(this.toEcodiag(), 'flux', up)
    let uncertainty = (edintensity.sups[0] - edintensity.infs[0]) / 2.0
    let intensity = new CarbonIntensity(
      edintensity.grey,
      uncertainty,
      ComputerDevice.type2group(this.type)
    )
    this.intensity = intensity
    return intensity
  }

  static createFromObj (device) {
    return new ComputerDevice(
      device.id,
      device.type,
      device.model,
      device.amount,
      device.acquisitionYear,
      device.ecodiagObject
    )
  }

  static exportHeader (sep = '\t') {
    return [
      'type',
      'model',
      'amount',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = ComputerDevice.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static okToSubmit (items, module = null) {
    return items.length > 0
  }

  static compute (devices) {
    let intensities = new CarbonIntensities()
    for (let device of devices) {
      let intensity = device.getCarbonIntensity()
      intensities.add(intensity)
    }
    return {
      'intensity': intensities.sum()
    }
  }

  static fromEcodiag (ecodiagObject) {
    let id = null
    if (Object.keys(ecodiagObject).includes('id')) {
      id = ecodiagObject.id
    }
    return new ComputerDevice(
      id,
      ecodiagObject.type,
      ecodiagObject.model,
      ecodiagObject.nb,
      ecodiagObject.year,
      ecodiagObject
    )
  }
}
