import Commute from '@/models/carbon/Commute.js'
import { DEFAULT_SETTINGS } from '@/stores/constants.js'

/**
 *
 * @param {Object} modifier key, value to modify in the default commute Object
 *
 * @returns {Commute}
 */
function commuteObj (modifier = undefined) {
  let commute = {
    'seqID': null,
    'position': null,
    'sections': [],
    'nWorkingDay': null,
    'nWorkingDay2': null,
    'message': null,
    'deleted': false
  }

  if (modifier === undefined) {
    modifier = {}
  }

  for (let [key, value] of Object.entries(modifier)) {
    commute[key] = value
  }
  return Commute.createFromObj(commute)
}

function zerosExcept (modifier) {
  // initialize an array with some value changed
  let result = new Array(Commute.TRAVELS_MODES.length).fill(0)
  if (modifier === undefined) {
    modifier = {}
  }

  for (let [mode, value] of Object.entries(modifier)) {
    result[Commute.TRAVELS_MODES.indexOf(mode)] = value
  }
  return result
}

describe('isPooling', () => {
  test('no section should return false', () => {
    let commute = commuteObj()
    expect(commute.isPooling).toBeFalsy()
  })

  // type1 carpooling
  test('mode=car, distance=1, pooling=2 should return true', () => {
    let commute = commuteObj({
      sections: [{
        mode: 'car',
        distance: 1,
        isDay2: false,
        pooling: 2
      }]
    })
    expect(commute.isPooling).toBeTruthy()
  })

  test('mode=motorbike, distance=1, pooling=2 should return true', () => {
    let commute = commuteObj({
      sections: [{
        mode: 'motorbike',
        distance: 1,
        isDay2: false,
        pooling: 2
      }]
    })
    expect(commute.isPooling).toBeTruthy()
  })

  test('mode=car, distance=1, pooling=2, isDay2=true should return true', () => {
    let commute = commuteObj({
      sections: [{
        mode: 'car',
        distance: 1,
        isDay2: true,
        pooling: 2
      }]
    })
    expect(commute.isPooling).toBeTruthy()
  })
})

describe('compute', () => {
  test('return value with no commutes: check anything but intensity', () => {
    let r = Commute.compute([], 0, 1, 1, 1, 1, 2042, DEFAULT_SETTINGS)
    // console.log(r)
    expect(r).toEqual(expect.objectContaining({
      totalPerCommute: [],
      status: expect.objectContaining({
        distances: {
          researcher: zerosExcept(),
          engineer: zerosExcept(),
          student: zerosExcept()
        } }),
      nbAnswers: { researcher: 0, engineer: 0, student: 0 },
      cdays: { researcher: 0, engineer: 0, student: 0 }
    }))
  })

  test('return value with [{car=1, engine=diesel, nbWorkingDay=5, position=engineer}] -- 100% answers: check anything but intensity', () => {
    let commute = commuteObj({
      nWorkingDay: 5,
      position: 'engineer',
      sections: [{
        mode: 'car',
        distance: 10,
        engine: 'diesel',
        isDay2: false,
        pooling: 1
      }]
    })

    let r = Commute.compute([commute], 0, 1, 1, 1, 1, 2042, DEFAULT_SETTINGS)
    // console.log(r)
    expect(r).toEqual(expect.objectContaining({
      // TODO
      // totalPerCommute
      // TODO
      // intensity
      status: expect.objectContaining({
        distances: {
          researcher: zerosExcept(),
          engineer: zerosExcept({ car: 10 * 5 * 41 }),
          student: zerosExcept()
        },
        volumePerMode: {
          researcher: zerosExcept(),
          engineer: zerosExcept({ car: 41 * 5 }),
          student: zerosExcept()
        }
      }),
      nbAnswers: { researcher: 0, engineer: 1, student: 0 },
      cdays: { researcher: 0, engineer: 41 * 5 * 1, student: 0 }
    })) // expect
  })
})

test('return value with [{car=1, engine=diesel, nbWorkingDay=5, position=engineer}] -- 50% answers: check anything but intensity', () => {
  let commute = commuteObj({
    nWorkingDay: 5,
    position: 'engineer',
    sections: [{
      mode: 'car',
      distance: 10,
      engine: 'diesel',
      isDay2: false,
      pooling: 1
    }]
  })

  // Consider having 1 answer over 2 engineers (50%)
  let r = Commute.compute([commute], 0, 1, 1, 2, 1, 2042, DEFAULT_SETTINGS)
  // console.log(r)
  expect(r).toEqual(expect.objectContaining({
    // TODO
    // totalPerCommute
    // TODO
    // intensity
    status: expect.objectContaining({
      distances: {
        researcher: zerosExcept(),
        // extrapolate to all the engineer : x2
        engineer: zerosExcept({ car: 2 * 10 * 5 * 41 }),
        student: zerosExcept()
      },
      volumePerMode: {
        researcher: zerosExcept(),
        engineer: zerosExcept({ car: 2 * 5 * 41 }),
        student: zerosExcept()
      }
    }),
    nbAnswers: { researcher: 0, engineer: 1, student: 0 },
    cdays: { researcher: 0, engineer: 41 * 5 * 1, student: 0 }
  })) // expect
})
