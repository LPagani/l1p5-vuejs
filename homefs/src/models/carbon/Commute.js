/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'
import CommuteSection from '@/models/carbon/CommuteSection.js'
import Survey from '@/models/carbon/Survey.js'

export default class Commute extends Survey {
  get hasWorkingDay2 () {
    let hasWorkingDay2 = false
    for (let section of this.sections) {
      if (section.distance > 0 && section.isDay2) {
        hasWorkingDay2 = true
        break
      }
    }
    return hasWorkingDay2
  }

  get isPooling () {
    let isPooling = false
    for (let section of this.sections) {
      if (section.distance > 0 && section.isPooling) {
        isPooling = true
        break
      }
    }
    return isPooling
  }

  get nWorkingDay1 () {
    let nWorkingDay1 = this.nWorkingDay
    if (this.hasWorkingDay2) {
      nWorkingDay1 = this.nWorkingDay - this.nWorkingDay2
    }
    return nWorkingDay1
  }

  get averageWeekDistance () {
    if (this.nWorkingDay > 0) {
      let total = 0
      for (let section of this.sections) {
        if (!section.isDay2) {
          total += this.nWorkingDay1 * parseFloat(section.distance)
        } else {
          total += this.nWorkingDay2 * parseFloat(section.distance)
        }
      }
      return total / this.nWorkingDay
    } else {
      return 0
    }
  }

  get weeklyDistance1PerMode () {
    let distances = new Array(Commute.TRAVELS_MODES.length).fill(0)
    for (let section of this.sections) {
      if (!section.isDay2) {
        let index = Commute.TRAVELS_MODES.indexOf(section.mode)
        distances[index] += parseFloat(section.distance)
      }
    }
    return distances
  }

  get weeklyDistance2PerMode () {
    let distances = new Array(Commute.TRAVELS_MODES.length).fill(0)
    for (let section of this.sections) {
      if (section.isDay2) {
        let index = Commute.TRAVELS_MODES.indexOf(section.mode)
        distances[index] += parseFloat(section.distance)
      }
    }
    return distances
  }

  /**
   * Get number of days per mode
   */
  get commutingDaysPerMode () {
    let cdays = Array(Commute.TRAVELS_MODES.length).fill(0)
    for (let section of this.sections) {
      let index = Commute.TRAVELS_MODES.indexOf(section.mode)
      if (!section.isDay2) {
        cdays[index] += this.nWorkingDay1
      } else {
        cdays[index] += this.nWorkingDay2
      }
    }
    return cdays
  }

  addSection (section) {
    if (section instanceof CommuteSection) {
      this.sections.push(section)
    } else {
      this.sections.push(CommuteSection.createFromObj(section))
    }
  }

  hasMode (mode, isDay2 = false) {
    let index = this.sections.findIndex(obj => obj.mode === mode && obj.isDay2 === isDay2)
    return index !== -1
  }

  hasSections (isDay2 = false) {
    return this.sections.filter(section => section.isDay2 === isDay2).length > 0
  }

  toString (sep = '\t') {
    let engine = null
    let motorbikepooling = 0
    let carpooling = 0
    let engine2 = null
    let motorbikepooling2 = 0
    let carpooling2 = 0
    for (let section of this.sections) {
      if (!section.isDay2 && section.mode === 'car') {
        engine = section.engine
        carpooling = section.pooling
      } else if (!section.isDay2 && section.mode === 'motorbike') {
        motorbikepooling = section.pooling
      } else if (section.isDay2 && section.mode === 'car') {
        engine2 = section.engine
        carpooling2 = section.pooling
      } else if (section.isDay2 && section.mode === 'motorbike') {
        motorbikepooling2 = section.pooling
      }
    }
    return [
      this.seqID,
      this.deleted,
      this.nWorkingDay,
      this.position,
      engine,
      motorbikepooling,
      carpooling,
      this.weeklyDistance1PerMode.map(val => Math.round(val)).join(sep),
      this.nWorkingDay2,
      engine2,
      motorbikepooling2,
      carpooling2,
      this.weeklyDistance2PerMode.map(val => Math.round(val)).join(sep),
      this.sectionsIntensities.map(obj => Math.round(obj.intensity)).join(sep),
      Math.round(this.sectionsIntensities.map(obj => obj.intensity).reduce(function (a, b) {
        return a + b
      }, 0)),
      this.message
    ].join(sep)
  }

  getNumberOfWorkedWeeks (settings, year) {
    let nbWeeks = settings.filter(obj => obj.name === 'NUMBER_OF_WORKED_WEEK.default')[0].value
    let years = settings.map(a => a.name.slice(-4))
    year = year.toString()
    if (years.includes(year)) {
      nbWeeks = settings.filter(obj => obj.name.endsWith(year))[0].value
    }
    return nbWeeks
  }

  getAnnualDistancesPerMode (year, settings) {
    let distances = new Array(Commute.TRAVELS_MODES.length).fill(0)
    const nbWeeks = this.getNumberOfWorkedWeeks(settings, year)
    for (let section of this.sections) {
      let index = Commute.TRAVELS_MODES.indexOf(section.mode)
      if (!section.isDay2) {
        distances[index] += section.distance * this.nWorkingDay1 * nbWeeks
      } else {
        distances[index] += section.distance * this.nWorkingDay2 * nbWeeks
      }
    }
    return distances
  }

  getAnnualDistance (settings, year = null) {
    return this.getAnnualDistancesPerMode(year, settings).reduce((a, b) => a + b)
  }

  getTotalIntensity (settings, year = null, citySize = 0) {
    let intensities = new CarbonIntensities()
    for (let intensity of this.getCarbonIntensity(citySize, settings, year)) {
      intensities.add(intensity)
    }
    return intensities.sum()
  }

  getCarbonIntensity (citySize, settings, year = null) {
    let intensities = new Array(Commute.TRAVELS_MODES.length).fill(0)
    const nbWeeks = this.getNumberOfWorkedWeeks(settings, year)
    for (let i in intensities) {
      intensities[i] = new CarbonIntensities()
    }
    if (!this.deleted) {
      for (let section of this.sections) {
        let anualFactor = this.nWorkingDay1 * nbWeeks
        if (section.isDay2) {
          anualFactor = this.nWorkingDay2 * nbWeeks
        }
        let index = Commute.TRAVELS_MODES.indexOf(section.mode)
        intensities[index].add(
          section.getCarbonIntensity(citySize, year).multiply(anualFactor)
        )
      }
    }
    // to correctly compute the uncertainty propagation, returns
    // CarbonIntensities instead of CarbonIntensity object
    // return intensities.map(obj => obj.sum())
    this.sectionsIntensities = intensities
    return intensities
  }

  toDatabase () {
    let nWorkingDay2 = 0
    if (this.nWorkingDay2 !== null) {
      nWorkingDay2 = this.nWorkingDay2
    }
    return {
      'id': this.id,
      'seqID': this.seqID,
      'position': this.position,
      'sections': this.sections,
      'nWorkingDay': this.nWorkingDay,
      'nWorkingDay2': nWorkingDay2,
      'message': this.message,
      'deleted': this.deleted
    }
  }

  static get TRAVELS_MODES () {
    return [
      'walking',
      'bike',
      'ebike',
      'escooter',
      'motorbike',
      'car',
      'bus',
      'busintercity',
      'tram',
      'train',
      'expressrailway',
      'subway'
    ]
  }

  static ICONS (mode) {
    return {
      'walking': 'walk',
      'bike': 'bicycle',
      'ebike': 'electric-bike',
      'escooter': 'electric-scooter',
      'motorbike': 'motorcycle',
      'car': 'car',
      'bus': 'bus',
      'busintercity': 'bus',
      'tram': 'tram',
      'train': 'train',
      'expressrailway': 'rer',
      'subway': 'subway'
    }[mode]
  }

  static ICONS_PACK (mode) {
    return {
      'walking': 'icomoon',
      'bike': 'fa',
      'ebike': 'icomoon',
      'escooter': 'icomoon',
      'motorbike': 'icomoon',
      'car': 'fa',
      'bus': 'fa',
      'busintercity': 'fa',
      'tram': 'icomoon',
      'train': 'fa',
      'expressrailway': 'icomoon',
      'subway': 'icomoon'
    }[mode]
  }

  static MAX_DISTANCES (mode) {
    return {
      'walking': 60,
      'bike': 200,
      'ebike': 200,
      'escooter': 200,
      'motorbike': 2000,
      'car': 2000,
      'bus': 2000,
      'busintercity': 2000,
      'tram': 300,
      'train': 2000,
      'expressrailway': 2000,
      'subway': 250
    }[mode]
  }

  static get POSITIONS () {
    return [
      Commute.POSITION_RESEARCHER,
      Commute.POSITION_ENGINEER,
      Commute.POSITION_STUDENT
    ]
  }

  static get POSITION_RESEARCHER () {
    return 'researcher'
  }

  static get POSITION_ENGINEER () {
    return 'engineer'
  }

  static get POSITION_STUDENT () {
    return 'student'
  }

  static createFromObj (obj) {
    return new Commute(
      obj.id,
      obj.seqID,
      obj.position,
      obj.sections,
      obj.meals,
      obj.nWorkingDay,
      obj.nWorkingDay2,
      obj.nCateringMeal,
      obj.message,
      obj.deleted
    )
  }

  static createEmpty () {
    return new Commute(
      null,
      null,
      null,
      [],
      [],
      null,
      null,
      null,
      null,
      false
    )
  }

  static initiStatusTable (length, fillIntensities = false) {
    let table = {}
    for (let position of Commute.POSITIONS) {
      table[position] = new Array(length).fill(0)
    }
    if (fillIntensities) {
      for (let key of Object.keys(table)) {
        for (let i in table[key]) {
          table[key][i] = new CarbonIntensities()
        }
      }
      return table
    } else {
      return table
    }
  }

  static exportHeader (sep = '\t') {
    let header = [
      'seqid',
      'deleted',
      'commuting.days',
      'position',
      'engine.std1',
      'motorbikepooling.std1',
      'carpooling.std1'
    ]
    for (let mode of Commute.TRAVELS_MODES) {
      header.push(mode + '.std1.km')
    }
    header.push('commuting.std2.days')
    header.push('engine.std2')
    header.push('motorbikepooling.std2')
    header.push('carpooling.std2')
    for (let mode of Commute.TRAVELS_MODES) {
      header.push(mode + '.std2.km')
    }
    for (let mode of Commute.TRAVELS_MODES) {
      header.push(mode + '.kg.co2e')
    }
    header.push('total.kg.co2e')
    header.push('message')
    return header.join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = Commute.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static okToSubmit (items, module = null) {
    return items.length > 0
  }

  static compute (
    commutes,
    citySize,
    nresearcher,
    nteacher,
    nengineer,
    nstudent,
    year,
    settings
  ) {
    let intensities = Commute.initiStatusTable(Commute.TRAVELS_MODES.length, true)
    let distances = Commute.initiStatusTable(Commute.TRAVELS_MODES.length)
    // hold the cumulative number of day each travel mode is used indexed by the position
    let volumePerMode = Commute.initiStatusTable(Commute.TRAVELS_MODES.length)
    let cdays = {}
    let nbAnswers = {}
    for (let position of Commute.POSITIONS) {
      cdays[position] = 0
      nbAnswers[position] = 0
    }
    let totalPerCommute = []
    for (let commute of commutes) {
      let intensity = commute.getCarbonIntensity(citySize, settings, year)
      if (!commute.deleted) {
        let cDistances = commute.getAnnualDistancesPerMode(year, settings)
        let nDaysPermode = commute.commutingDaysPerMode
        totalPerCommute.unshift([commute.seqID].concat(intensity))
        nbAnswers[commute.position] += 1
        cdays[commute.position] += commute.nWorkingDay * commute.getNumberOfWorkedWeeks(settings, year)
        for (let index = 0; index < Commute.TRAVELS_MODES.length; index++) {
          intensities[commute.position][index].add(intensity[index])
          distances[commute.position][index] += cDistances[index]
          volumePerMode[commute.position][index] += nDaysPermode[index] * commute.getNumberOfWorkedWeeks(settings, year)
        }
      }
    }
    // convert CarbonIntensities to CarbonIntensity for normalization
    Object.keys(intensities).map(function (key) {
      intensities[key] = intensities[key].map(obj => obj.sum())
    })
    // normalize intensity by the number of lab members
    for (let position of Object.keys(intensities)) {
      for (let index = 0; index < Commute.TRAVELS_MODES.length; index++) {
        let normalizationFactor = 0
        if (nbAnswers[position] !== 0 && position === Commute.POSITION_RESEARCHER) {
          normalizationFactor = (nresearcher + nteacher) / nbAnswers[position]
        } else if (nbAnswers[position] !== 0 && position === Commute.POSITION_ENGINEER) {
          normalizationFactor = nengineer / nbAnswers[position]
        } else if (nbAnswers[position] !== 0 && position === Commute.POSITION_STUDENT) {
          normalizationFactor = nstudent / nbAnswers[position]
        }
        intensities[position][index] = intensities[position][index].multiply(normalizationFactor)
        distances[position][index] = distances[position][index] * normalizationFactor
        volumePerMode[position][index] = volumePerMode[position][index] * normalizationFactor
      }
    }
    let total = new CarbonIntensities()
    for (let position of Object.keys(intensities)) {
      for (let index = 0; index < Commute.TRAVELS_MODES.length; index++) {
        total.add(intensities[position][index])
      }
    }
    return {
      'intensity': total.sum(),
      'totalPerCommute': totalPerCommute,
      'status': {
        'intensity': intensities,
        'distances': distances,
        'volumePerMode': volumePerMode
      },
      'nbAnswers': nbAnswers,
      'cdays': cdays
    }
  }
}
