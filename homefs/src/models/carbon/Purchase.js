/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { CarbonIntensity, CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'
import PURCHASES_FACTORS from '@/../data/emissionFactors/purchasesFactors.json'
import Modules from '@/models/Modules.js'
import EmissionFactor from '@/models/carbon/EmissionFactor.js'

const TRAVELLING_EXPENSES_CODES = ['xa01', 'xa02', 'xa11', 'xa12']
const WARNING_RATE = 20

export default class Purchase {
  constructor (id, code, amount, source = null) {
    this.id = id
    this.setCode(code)
    this.amount = this.setAmount(amount)
    this.source = source
    this.intensity = new CarbonIntensity()
  }

  /**
   * Test whether 'item' is equal to 'this'
   * Note that 'this' can come from different sources:
   * - from the DB (it will be given an id)
   * - from the user input (it won't have an id yet)
   * This is used in the PurchaseCollection to make sure we differentiate
   * between the two sources (and not mix an update/delete between items of
   * different sources)
   * @param {*} purchase
   * @returns a boolean, true iff items are equals
   */
  isEqualTo (purchase) {
    return (this.id === purchase.id && this.code === purchase.code && this.amount === purchase.amount && this.source === purchase.source)
  }

  get score () {
    if (!isNaN(this.amount) && Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      // valid
      return 1
    }
    // invalid
    return 2
  }

  get module () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].module
    } else {
      return null
    }
  }

  get category () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].category
    } else {
      return null
    }
  }

  get categoryGHGP () {
    let catGHGP = '3.1'
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      if (PURCHASES_FACTORS[this.code].catGHGP !== null) {
        catGHGP = PURCHASES_FACTORS[this.code].catGHGP
      }
    }
    return catGHGP
  }

  get categoryV5 () {
    let catV5 = '4.1'
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      if (PURCHASES_FACTORS[this.code].catV5 !== null) {
        catV5 = PURCHASES_FACTORS[this.code].catV5
      }
    }
    return catV5
  }

  get description () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return null
    }
  }

  toString (sep = '\t') {
    return [
      this.code.toUpperCase(),
      this.module,
      this.categoryGHGP,
      this.categoryV5,
      this.category,
      this.description,
      this.amount,
      Math.round(this.intensity.intensity),
      Math.round(this.intensity.uncertainty)
    ].join(sep)
  }

  isOther () {
    return this.module !== Purchase.MODULE_NAME
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return false
  }

  isInvalid () {
    return this.score === 2
  }

  setCode (value) {
    let lvalue = value.toLowerCase().replace('.', '').trim()
    this.code = lvalue
  }

  setAmount (value) {
    let amount = value
    if (typeof amount === 'string') {
      amount = amount.replace(',', '.').trim().replace(/\s/g, '')
    }
    if (!isNaN(amount)) {
      amount = parseFloat(amount)
    }
    return amount
  }

  getCarbonIntensity (year = null) {
    let ef = this.getEmissionFactor(this.code, year)
    let intensity = new CarbonIntensity(
      this.amount * ef.total.total,
      this.amount * ef.total.total * ef.total.uncertainty,
      ef.group
    )
    this.intensity = intensity
    return intensity
  }

  getEmissionFactor (code, year) {
    let ef = EmissionFactor.createFromObj(PURCHASES_FACTORS[code])
    return ef.getFactor(year)
  }

  getFactorDescription () {
    if (Object.keys(PURCHASES_FACTORS).includes(this.code)) {
      return PURCHASES_FACTORS[this.code].description_FR
    } else {
      return '-'
    }
  }

  getCodeFromDescription (value) {
    let lvalue = value.toLowerCase()
    let fcode = null
    for (let code of Object.keys(PURCHASES_FACTORS)) {
      if (PURCHASES_FACTORS[code].description_FR.toLowerCase() === lvalue) {
        fcode = code
      }
    }
    return fcode
  }

  toDatabase () {
    return {
      'code': this.code,
      'amount': this.amount
    }
  }

  static isACode (code) {
    let regex = new RegExp('^[a-z]{2}[0-9]{2}$')
    return regex.test(code.toLowerCase())
  }

  static reduce (items) {
    let reducedPurchases = []
    for (let item of items) {
      let index = reducedPurchases.findIndex(obj => obj.code === item.code)
      if (index < 0) {
        reducedPurchases.push(item)
      } else {
        reducedPurchases[index].amount += item.amount
      }
    }
    return reducedPurchases
  }

  static getTravellingExpensesCodes () {
    return TRAVELLING_EXPENSES_CODES
  }

  static getWarningRate () {
    return WARNING_RATE
  }

  static getCatGHGP () {
    let catGHGP = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].catGHGP
    })
    return Array.from(new Set(catGHGP)).filter(val => val !== null)
  }

  static getCatV5 () {
    let catV5 = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].catV5
    })
    return Array.from(new Set(catV5)).filter(val => val !== null)
  }

  static getCategories () {
    let categories = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].category
    })
    return Array.from(new Set(categories)).filter(val => val !== null)
  }

  static getCodes () {
    let codes = Object.keys(PURCHASES_FACTORS)
    return Array.from(new Set(codes))
  }

  static getDescriptions () {
    let descriptions = Object.keys(PURCHASES_FACTORS).map(function (code) {
      return PURCHASES_FACTORS[code].description_FR
    })
    return Array.from(new Set(descriptions))
  }

  static getDescription (code) {
    if (Object.keys(PURCHASES_FACTORS).includes(code)) {
      return PURCHASES_FACTORS[code].description_FR
    } else {
      return null
    }
  }

  static get MODULE_NAME () {
    return Modules.PURCHASES
  }

  static createFromObj (item) {
    return new Purchase(
      item.id,
      item.code,
      item.amount,
      item.source
    )
  }

  static exportHeader (sep = '\t') {
    return [
      'code',
      'module',
      'ghg.source',
      'v5scope.source',
      'category',
      'description',
      'amount.euro',
      'emission.kg.co2e',
      'uncertainty.kg.co2e'
    ].join(sep)
  }

  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let headerValues = []
    if (header) {
      headerValues = Purchase.exportHeader(sep)
    }
    return [
      headerValues,
      ...items.map(function (item) {
        let val = ''
        if (extraColValue) {
          val += extraColValue + sep
        }
        return val + item.toString(sep)
      })
    ]
      .join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  static okToSubmit (items, module = null) {
    return items.length > 0
  }

  static compute (items, year) {
    let intensities = new CarbonIntensities()
    let catGHGP = {}
    let catV5 = {}
    let categories = {}
    let totalXA = 0
    for (let item of items) {
      let intensity = item.getCarbonIntensity(year)
      if (item.module === Purchase.MODULE_NAME) {
        intensities.add(intensity)
        if (Object.keys(catGHGP).includes(item.categoryGHGP.toString())) {
          catGHGP[item.categoryGHGP.toString()].add(intensity)
        } else {
          catGHGP[item.categoryGHGP.toString()] = new CarbonIntensities()
          catGHGP[item.categoryGHGP.toString()].add(intensity)
        }
        if (Object.keys(catV5).includes(item.categoryV5.toString())) {
          catV5[item.categoryV5.toString()].add(intensity)
        } else {
          catV5[item.categoryV5.toString()] = new CarbonIntensities()
          catV5[item.categoryV5.toString()].add(intensity)
        }
        if (Object.keys(categories).includes(item.category)) {
          categories[item.category].add(intensity)
        } else {
          categories[item.category] = new CarbonIntensities()
          categories[item.category].add(intensity)
        }
        if (TRAVELLING_EXPENSES_CODES.includes(item.code)) {
          totalXA += intensity.intensity
        }
      }
    }
    Object.keys(categories).map(function (key) {
      categories[key] = categories[key].sum()
    })
    Object.keys(catGHGP).map(function (key) {
      catGHGP[key] = catGHGP[key].sum()
    })
    Object.keys(catV5).map(function (key) {
      catV5[key] = catV5[key].sum()
    })
    return {
      'intensity': intensities.sum(),
      'catGHGP': catGHGP,
      'catV5': catV5,
      'categories': categories,
      'totalXA': totalXA
    }
  }
}
