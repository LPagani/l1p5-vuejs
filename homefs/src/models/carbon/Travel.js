/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import TravelSection from '@/models/carbon/TravelSection'
import { CarbonIntensities } from '@/models/carbon/CarbonIntensity.js'

import ISO3166 from '@/../data/utils/ISO3166.json'

export const PURPOSE_FIELD_STUDY = 'field.study'
export const PURPOSE_CONFERENCE = 'conference'
export const PURPOSE_SEMINAR = 'seminar'
export const PURPOSE_TEACHING = 'teaching'
export const PURPOSE_COLLABORATION = 'collaboration'
export const PURPOSE_VISIT = 'visit'
export const PURPOSE_RESEARCH_MANAGEMENT = 'research.management'
export const PURPOSE_OTHER = 'other'
export const PURPOSE_UNKNOWN = 'unknown'

export const STATUS_GUEST = 'guest'
export const STATUS_RESEARCHER = 'researcher'
export const STATUS_ENGINEER = 'engineer'
export const STATUS_STUDENT = 'student'
export const STATUS_UNKNOWN = 'unknown'

export const MODE_PLANE = 'plane'
export const MODE_TRAIN = 'train'
export const MODE_CAR = 'car'
export const MODE_CAB = 'cab'
export const MODE_BUS = 'busintercity'
export const MODE_TRAM = 'tram'
export const MODE_RER = 'rer'
export const MODE_SUBWAY = 'subway'
export const MODE_FERRY = 'ferry'

const MODES_DEPLACEMENTS = {
  [MODE_PLANE]: 1.0,
  [MODE_TRAIN]: 1.2,
  [MODE_CAR]: 1.3,
  [MODE_CAB]: 1.3,
  [MODE_BUS]: 1.5,
  [MODE_TRAM]: 1.5,
  [MODE_RER]: 1.2,
  [MODE_SUBWAY]: 1.7,
  [MODE_FERRY]: 1.0
}

const PURPOSE_DICTIONARY = {
  [PURPOSE_FIELD_STUDY]: PURPOSE_FIELD_STUDY,
  'field study': PURPOSE_FIELD_STUDY,
  'etude de terrain': PURPOSE_FIELD_STUDY,
  'etude terrain': PURPOSE_FIELD_STUDY,
  [PURPOSE_CONFERENCE]: PURPOSE_CONFERENCE,
  'colloque-congres': PURPOSE_CONFERENCE,
  'colloque': PURPOSE_CONFERENCE,
  'congres': PURPOSE_CONFERENCE,
  [PURPOSE_SEMINAR]: PURPOSE_SEMINAR,
  'seminaire': PURPOSE_SEMINAR,
  [PURPOSE_TEACHING]: PURPOSE_TEACHING,
  'enseignement': PURPOSE_TEACHING,
  [PURPOSE_COLLABORATION]: PURPOSE_COLLABORATION,
  [PURPOSE_VISIT]: PURPOSE_VISIT,
  'visite': PURPOSE_VISIT,
  [PURPOSE_RESEARCH_MANAGEMENT]: PURPOSE_RESEARCH_MANAGEMENT,
  'research management': PURPOSE_RESEARCH_MANAGEMENT,
  'administration de la recherche': PURPOSE_RESEARCH_MANAGEMENT,
  'administration': PURPOSE_RESEARCH_MANAGEMENT,
  [PURPOSE_OTHER]: PURPOSE_OTHER,
  'autre': PURPOSE_OTHER,
  [PURPOSE_UNKNOWN]: PURPOSE_UNKNOWN,
  '': PURPOSE_UNKNOWN
}

const STATUS_DICTIONARY = {
  [STATUS_GUEST]: STATUS_GUEST,
  'personne invitee': STATUS_GUEST,
  'invited person': STATUS_GUEST,
  [STATUS_RESEARCHER]: STATUS_RESEARCHER,
  'chercheur.e-ec': STATUS_RESEARCHER,
  [STATUS_ENGINEER]: STATUS_ENGINEER,
  'ita': STATUS_ENGINEER,
  'engineers-technicians-administrative staff': STATUS_ENGINEER,
  'engineer': STATUS_ENGINEER,
  'technician': STATUS_ENGINEER,
  'administrative': STATUS_ENGINEER,
  [STATUS_STUDENT]: STATUS_STUDENT,
  'doc-post doc': STATUS_STUDENT,
  'phd-post doctoral fellow': STATUS_STUDENT,
  'phd': STATUS_STUDENT,
  'post-doc': STATUS_STUDENT,
  'post doc': STATUS_STUDENT,
  [STATUS_UNKNOWN]: STATUS_UNKNOWN,
  '': STATUS_UNKNOWN
}

const MODE_DICTIONARY = {
  'avion': MODE_PLANE,
  'plane': MODE_PLANE,
  'train': MODE_TRAIN,
  'voiture': MODE_CAR,
  'location de vehicule': MODE_CAR,
  'vehicule personnel': MODE_CAR,
  'car': MODE_CAR,
  'taxi': MODE_CAB,
  'cab': MODE_CAB,
  'bus': MODE_BUS,
  'busintercity': MODE_BUS,
  'bus intercity': MODE_BUS,
  'bus intercite': MODE_BUS,
  'tram': MODE_TRAM,
  'tramway': MODE_TRAM,
  'rer': MODE_RER,
  'metro': MODE_SUBWAY,
  'subway': MODE_SUBWAY,
  'ferry': MODE_FERRY
}

const ICONS = {
  [MODE_PLANE]: 'plane',
  [MODE_TRAIN]: 'train',
  [MODE_CAR]: 'car',
  [MODE_CAB]: 'cab',
  [MODE_BUS]: 'bus',
  [MODE_TRAM]: 'tram',
  [MODE_RER]: 'rer',
  [MODE_SUBWAY]: 'subway',
  [MODE_FERRY]: 'ship'
}

const ICONS_PACK = {
  [MODE_PLANE]: 'fa',
  [MODE_TRAIN]: 'fa',
  [MODE_CAR]: 'fa',
  [MODE_CAB]: 'fa',
  [MODE_BUS]: 'fa',
  [MODE_TRAM]: 'icomoon',
  [MODE_RER]: 'icomoon',
  [MODE_SUBWAY]: 'icomoon',
  [MODE_FERRY]: 'fa'
}

export default class Travel {
  constructor (
    names, date, sections, amount, purpose = null,
    status = null, source = null
  ) {
    if (Array.isArray(names)) {
      this.names = names
    } else {
      this.names = [names]
    }
    this.setDate(date)
    this.sections = sections.map((obj) => TravelSection.createFromObj(obj))
    this.setAmount(amount)
    this.setPurpose(purpose)
    this.setStatus(status)
    this.source = source
    if (this.isFromDatabase()) {
      this.source = 'database'
    }
  }

  setDate (value) {
    /* let date = null
    try {
      let dateParts = value.split('/')
      if (dateParts.length === 3) {
        date = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0]
      } else {
        date = value
      }
      let d = new Date(date)
      if (isNaN(d.getTime())) {
        this.isValid = false
      }
    } catch (error) {
      this.isValid = false
    }
    this.date = date */
    this.date = value
  }

  setAmount (value) {
    this.amount = 1
    if (!isNaN(value) && value !== '') {
      this.amount = parseInt(value)
    }
  }

  setPurpose (value) {
    this.purpose = Travel.getTravelPurpose(value)
  }

  setStatus (value) {
    this.status = Travel.getStatus(value)
  }

  addSection (section) {
    if (section instanceof TravelSection) {
      this.sections.push(section)
    } else {
      this.sections.push(TravelSection.createFromObj(section))
    }
  }

  sortSections () {
    let self = this
    // Keep the number of predecessors in cache (used by findStartingPoint)
    let nbPredecessors = self.sections.map(s1 =>
      self.sections.filter(s2 => s2.destinationCity === s1.departureCity).length)

    // Among the subset of sections indexed by sectionIds,
    // returns the index of first section having no predecessors if any,
    // or simply the index of the first section otherwise
    function findStartingPoint (sectionIds) {
      for (let i of sectionIds) {
        if (nbPredecessors[i] === 0) {
          return i
        }
      }
      return sectionIds[0]
    }
    // Recursively push the next sections indices to the current chain.
    // The array 'visited' is a boolean mask having the same size as this.sections.
    // It is used to speed up testing whether a section index is already present in the current chain.
    function formLongestChain (chain = [], visited = undefined, noRestart = false) {
      if (chain.length === 0) {
        // Init
        let start = findStartingPoint(self.sections.map((v, i) => i))
        visited = self.sections.map(() => false)
        visited[start] = true
        return formLongestChain([start], visited)
      }

      let lastSection = self.sections[chain[chain.length - 1]]
      let successors = self.sections
        .map(function (sec, i) { return { id: i, departureCity: sec.departureCity } })
        .filter((sec, i) => (!visited[i]) && sec.departureCity === lastSection.destinationCity)

      if (successors.length === 1) {
        let nextId = successors[0].id
        visited[nextId] = true
        chain.push(nextId)
        return formLongestChain(chain, visited)
      } else if (successors.length > 0) {
        // If there are multiple branches, explore them all
        // and keep the one leading to longest connected chain
        let newChain = chain
        let newVisited = visited
        for (let next of successors) {
          let tmpChain = [...chain, next.id]
          let tmpVisited = [...visited]
          tmpVisited[next.id] = true
          tmpChain = formLongestChain(tmpChain, tmpVisited, noRestart = true)
          if (tmpChain.length > newChain.length) {
            newChain = tmpChain
            newVisited = tmpVisited
          }
        }
        return formLongestChain(newChain, newVisited)
      } else if ((!noRestart) && (chain.length < self.sections.length)) {
        // restart from a new starting point
        let start = findStartingPoint(self.sections.map((v, i) => i).filter((i) => (!visited[i])))
        visited[start] = true
        chain.push(start)
        return formLongestChain(chain, visited)
      }
      return chain
    }
    let sortedChain = formLongestChain()
    this.sections = this.sections.map((e, i) => this.sections[sortedChain[i]])
  }

  get score () {
    let score = 1
    if (!this.isFromDatabase()) {
      for (let section of this.sections) {
        if (section.score === 2) {
          score = 2
          break
        }
      }
    }
    return score
  }

  isOther () {
    return false
  }

  isValid () {
    return this.score === 1
  }

  isIncomplete () {
    return false
  }

  isInvalid () {
    return this.score === 2
  }

  latLngIsValid () {
    let isValid = true
    for (let section of this.sections) {
      if (!section.latLngIsValid()) {
        isValid = false
      }
    }
    return isValid
  }

  isEqualTo (travel, checkAmount = true) {
    let isEqual = false
    if ((this.getDistance() === travel.getDistance() ||
        (isNaN(this.getDistance()) && isNaN(travel.getDistance()))) &&
      this.isRoundTrip() === travel.isRoundTrip() &&
      this.purpose === travel.purpose && this.status === travel.status &&
      this.source === travel.source && this.sections.length === travel.sections.length) {
      // FIXME: transportations aren't checked so same distance by car and by plane will be considered equal
      // depending on the context this might or might not be ok
      if (!checkAmount) {
        isEqual = true
      } else {
        if (this.amount === travel.amount) {
          isEqual = true
        }
      }
    }
    return isEqual
  }

  isRoundTrip () {
    let isRoundTrip = false
    for (let section of this.sections) {
      if (section.isRoundTrip) {
        isRoundTrip = true
      }
    }
    return isRoundTrip
  }

  isComplete () {
    let isComplete = true
    for (let section of this.sections) {
      if (section.departureCity === null || section.destinationCity === null || section.transportation === null) {
        isComplete = false
      }
    }
    return isComplete
  }

  getDistance () {
    let distance = 0
    for (let section of this.sections) {
      if (section.departureCity !== null && section.destinationCity !== null && section.transportation !== null) {
        distance += section.getCorrectedDistance()
      } else if (section.distance !== null) {
        distance += section.getCorrectedDistance()
      }
    }
    return distance
  }

  hasSectionWithPlane () {
    let withPlane = false
    for (let section of this.sections) {
      if (section.transportation === MODE_PLANE) {
        withPlane = true
        break
      }
    }
    return withPlane
  }

  getMainTransportation () {
    let transportation = null
    let maxDistance = 0
    for (let section of this.sections) {
      if (section.getCorrectedDistance() > maxDistance) {
        maxDistance = section.getCorrectedDistance()
        transportation = section.transportation
      }
    }
    return transportation
  }

  getDepartureCity () {
    return this.sections[0].departureCity
  }

  getDepartureCountry () {
    return this.sections[0].departureCountry
  }

  getDestinationCity () {
    let last = this.sections.length - 1
    return this.sections[last].destinationCity
  }

  getDestinationCountry () {
    let last = this.sections.length - 1
    return this.sections[last].destinationCountry
  }

  getCarbonIntensity (year) {
    // this is really fragile but kept for now for retro-compatibility until
    // this is better tested hints:
    // 1. seems we use the TravelSection object when simulating/filling the form
    // for a new travel in this case the geodessic distance uis computed on the
    // fly based on lat, lng. createFromObj does this.
    // 2. we also use travel section when dealing with more anonymous data (no
    // lat / lng ).
    //
    // A better design would be to have 2 distincts classes for each of the above use case.
    // but :/ for now:
    // - 05/06/23 msimonin add a behaviour when the distance of the section is here
    // (don't need to go through the geodesic calculation which will failed)

    let intensities = new CarbonIntensities()
    for (let section of this.sections) {
      if (section.departureCity !== null && section.destinationCity !== null && section.transportation !== null) {
        // case 1.
        let nSection = TravelSection.createFromObj(section)
        // be ware that this will recompute the geodesic distance with lat lng
        intensities.add(
          nSection.getCarbonIntensity(year).multiply(this.amount)
        )
      } else if (section.distance) {
        // case 2.
        intensities.add(
          section.getCarbonIntensity(year).multiply(this.amount)
        )
      }
    }
    return intensities.sum()
  }

  toDatabase () {
    let toDatabase = {
      'names': this.names,
      'location': this.location,
      'purpose': this.purpose,
      'status': this.status,
      'amount': this.amount,
      'sections': []
    }
    for (let section of this.sections) {
      toDatabase.sections.push(section.toDatabase())
    }
    return toDatabase
  }

  isFromDatabase () {
    let isFromDatabase = false
    if (this.sections.length > 0) {
      isFromDatabase = this.sections[0].departureCity === null &&
      this.sections[0].destinationCity === null &&
      this.sections[0].departureCountry === null &&
      this.sections[0].destinationCountry === null &&
      this.sections[0].departureCityLat === 0 &&
      this.sections[0].departureCityLng === 0 &&
      this.sections[0].destinationCityLat === 0 &&
      this.sections[0].destinationCityLng === 0
    }
    return isFromDatabase
  }

  static get MODE_PLANE () {
    return MODE_PLANE
  }

  static get MODE_TRAIN () {
    return MODE_TRAIN
  }

  static get MODE_CAR () {
    return MODE_CAR
  }

  static get MODE_CAB () {
    return MODE_CAB
  }

  static get MODE_BUS () {
    return MODE_BUS
  }

  static get MODE_TRAM () {
    return MODE_TRAM
  }

  static get MODE_RER () {
    return MODE_RER
  }

  static get MODE_SUBWAY () {
    return MODE_SUBWAY
  }

  static get MODE_FERRY () {
    return MODE_FERRY
  }

  static get PURPOSE_FIELD_STUDY () {
    return PURPOSE_FIELD_STUDY
  }

  static get PURPOSE_CONFERENCE () {
    return PURPOSE_CONFERENCE
  }

  static get PURPOSE_SEMINAR () {
    return PURPOSE_SEMINAR
  }

  static get PURPOSE_TEACHING () {
    return PURPOSE_TEACHING
  }

  static get PURPOSE_COLLABORATION () {
    return PURPOSE_COLLABORATION
  }

  static get PURPOSE_VISIT () {
    return PURPOSE_VISIT
  }

  static get PURPOSE_RESEARCH_MANAGEMENT () {
    return PURPOSE_RESEARCH_MANAGEMENT
  }

  static get PURPOSE_OTHER () {
    return PURPOSE_OTHER
  }

  static get PURPOSE_UNKNOWN () {
    return PURPOSE_UNKNOWN
  }

  static get STATUS_GUEST () {
    return STATUS_GUEST
  }

  static get STATUS_RESEARCHER () {
    return STATUS_RESEARCHER
  }

  static get STATUS_ENGINEER () {
    return STATUS_ENGINEER
  }

  static get STATUS_STUDENT () {
    return STATUS_STUDENT
  }

  static get STATUS_UNKNOWN () {
    return STATUS_UNKNOWN
  }

  static get purposes () {
    return [
      PURPOSE_FIELD_STUDY,
      PURPOSE_CONFERENCE,
      PURPOSE_SEMINAR,
      PURPOSE_TEACHING,
      PURPOSE_COLLABORATION,
      PURPOSE_VISIT,
      PURPOSE_RESEARCH_MANAGEMENT,
      PURPOSE_OTHER,
      PURPOSE_UNKNOWN
    ]
  }

  static get status () {
    return [
      STATUS_GUEST,
      STATUS_RESEARCHER,
      STATUS_ENGINEER,
      STATUS_STUDENT,
      STATUS_UNKNOWN
    ]
  }

  static hasPurposeOption (travels, source = null) {
    let nbWithPurpose = 0
    if (source === null) {
      for (let travel of travels) {
        if (travel.purpose !== null && travel.purpose !== Travel.PURPOSE_UNKNOWN) {
          nbWithPurpose += parseInt(travel.amount)
        }
      }
    } else {
      for (let travel of travels) {
        if (travel.purpose !== null && travel.purpose !== Travel.PURPOSE_UNKNOWN && travel.source === source) {
          nbWithPurpose += parseInt(travel.amount)
        }
      }
    }
    return nbWithPurpose > 0
  }

  static hasStatusOption (travels, source = null) {
    let nbWithStatus = 0
    if (source === null) {
      for (let travel of travels) {
        if (travel.status !== null && travel.status !== Travel.STATUS_UNKNOWN) {
          nbWithStatus += parseInt(travel.amount)
        }
      }
    } else {
      for (let travel of travels) {
        if (travel.status !== null && travel.status !== Travel.STATUS_UNKNOWN && travel.source === source) {
          nbWithStatus += parseInt(travel.amount)
        }
      }
    }
    return nbWithStatus > 0
  }

  static getTransportation (transportation) {
    let ctransportation = Travel.removeAccents(transportation, false).toLowerCase()
    if (ctransportation in MODE_DICTIONARY) {
      return MODE_DICTIONARY[ctransportation]
    } else {
      return null
    }
  }

  static getTravelPurpose (purpose) {
    let modifiedPurpose = Travel.removeAccents(purpose, false).toLowerCase()
    if (modifiedPurpose in PURPOSE_DICTIONARY) {
      return PURPOSE_DICTIONARY[modifiedPurpose]
    } else {
      return PURPOSE_UNKNOWN
    }
  }

  static getStatus (status) {
    let modifiedStatus = Travel.removeAccents(status, false).toLowerCase()
    if (modifiedStatus in STATUS_DICTIONARY) {
      return STATUS_DICTIONARY[modifiedStatus]
    } else {
      return STATUS_UNKNOWN
    }
  }

  static getIcon (mode) {
    return ICONS[mode]
  }

  static getIconPack (mode) {
    return ICONS_PACK[mode]
  }

  static get transportations () {
    return Object.keys(MODES_DEPLACEMENTS)
  }

  static get transportationsCorrectionFactors () {
    return MODES_DEPLACEMENTS
  }

  static removeAccents (str, tiret = true) {
    let fstr = str
    try {
      let accents = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž'
      let accentsOut = 'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz'
      str = str.split('')
      let strLen = str.length
      for (let i = 0; i < strLen; i++) {
        let x = accents.indexOf(str[i])
        if (x !== -1) {
          str[i] = accentsOut[x]
        }
      }
      if (tiret) {
        fstr = str.join('').replace('-', ' ')
      } else {
        fstr = str.join('')
      }
    } catch (error) {
      fstr = ''
    }
    return fstr
  }

  static iso3166Country (country) {
    let finalCountry = null
    if (typeof country === 'string') {
      if (country.length !== 2) {
        try {
          finalCountry = ISO3166.filter(obj => Travel.removeAccents(obj.nom.toLowerCase()) === Travel.removeAccents(country.toLowerCase()))[0].iso
        } catch (e) {
          finalCountry = null
        }
      } else {
        try {
          finalCountry = ISO3166.filter(obj => Travel.removeAccents(obj.iso.toLowerCase()) === Travel.removeAccents(country.toLowerCase()))[0].iso
        } catch (e) {
          finalCountry = null
        }
      }
    }
    return (finalCountry)
  }

  static initiStatutsTable (length, fillIntensities = false) {
    let table = {
      [Travel.STATUS_RESEARCHER]: new Array(length).fill(0),
      [Travel.STATUS_ENGINEER]: new Array(length).fill(0),
      [Travel.STATUS_STUDENT]: new Array(length).fill(0),
      [Travel.STATUS_GUEST]: new Array(length).fill(0),
      [Travel.STATUS_UNKNOWN]: new Array(length).fill(0)
    }
    if (fillIntensities) {
      for (let key of Object.keys(table)) {
        for (let i in table[key]) {
          table[key][i] = new CarbonIntensities()
        }
      }
      return table
    } else {
      return table
    }
  }

  static initiEmptyTable (length) {
    let table = new Array(length).fill(0)
    for (let i in table) {
      table[i] = new CarbonIntensities()
    }
    return table
  }

  static createFromObj (travel) {
    let ctravel = new Travel(
      travel.names,
      travel.date,
      [],
      travel.amount,
      travel.purpose,
      travel.status,
      travel.source
    )
    for (let section of travel.sections) {
      ctravel.addSection(section)
    }
    return ctravel
  }

  static reduce (travels) {
    let reducedTravels = []
    for (let travel of travels) {
      let index = reducedTravels.findIndex(obj => obj.isEqualTo(travel, false))
      if (index < 0) {
        reducedTravels.push(travel)
      } else {
        reducedTravels[index].names.push(...travel.names)
        // NOTE there's an invariant here that amount is the aggregated count of
        // travels and each travel as a name so the amount here is always must
        // remain equal to the length of names
        // this should work too
        // reducedTravels[index].amount = reducedTravels[index].names.length
        reducedTravels[index].amount += travel.amount
      }
    }
    return reducedTravels
  }

  static exportHeader (sep = '\t') {
    let travelHeader = [
      'name',
      'amount',
      'position', // status
      'purpose'
    ]
    let sectionHeader = TravelSection.exportHeader()

    let computedHeader = [
      'cumulative.distance.km',
      'cumulative.emission.kg.co2e',
      'cumulative.uncertainty.kg.co2e'
    ]

    return travelHeader.concat(sectionHeader, computedHeader).join(sep)
  }

  /**
   * String representation of a travel and its sections
   * This is suposed to be tsv based format with as many lines as travel
   * sections
   * @param {*} extraColValue
   * @param {*} sep
   * @returns String
   */
  toString (sep = '\t', extraColValue = null) {
    let travelCols = [
      this.names.join(','),
      this.amount,
      this.status,
      this.purpose
    ]
    return this.sections.map(s => {
      let secStr = s.toString(sep)
      // computed columns
      // this assumes the intensity is set (by s.getCarbonIntensity(year)) :/
      let si = s.intensity.multiply(this.amount)
      let computedCols = [
        this.amount * s.getFullDistance(),
        si.intensity,
        si.uncertainty
      ]
      let r = [
        travelCols.join(sep),
        secStr,
        computedCols.join(sep)
      ]
      if (extraColValue !== null) {
        // There is an implicit here that extraColValue must go in the first position
        // see superadmin/GES1point5.vue#373 (approx)
        r.unshift(extraColValue)
      }

      return r.join(sep)
    }).join('\n')
      .replace(/(^\[)|(\]$)/gm, '')
  }

  /**
   * Export a list of travels as a separated value field/values
   *
   * @param {*} items
   * @param {*} header
   * @param {*} extraColValue used to inject the ghgi id in superadmin/GES1point5
   * @param {*} sep
   * @returns
   */
  static exportToFile (items, header = true, extraColValue = null, sep = '\t') {
    let content = items.map(travel => travel.toString(sep, extraColValue))

    if (header) {
      content.unshift(Travel.exportHeader())
    } else {
      // FIXME keep the previous behaviour which adds an \n at the beginning when
      // header = false
      content.unshift('')
    }
    let s = content.join('\n').replace(/(^\[)|(\]$)/gm, '')
    return s
  }

  static okToSubmit (items, module = null) {
    return items.length > 0
  }

  static compute (travels, year) {
    let intensities = new CarbonIntensities()
    let transportationCI = Travel.initiEmptyTable(Travel.transportations.length)
    let transportationD = new Array(Travel.transportations.length).fill(0)
    let totalPurposesCI = Travel.initiEmptyTable(Travel.purposes.length)
    let totalPurposesD = new Array(Travel.purposes.length).fill(0)
    let statusCI = Travel.initiStatutsTable(Travel.transportations.length, true)
    let statusD = Travel.initiStatutsTable(Travel.transportations.length)
    let purposesCI = Travel.initiStatutsTable(Travel.purposes.length, true)
    let purposesD = Travel.initiStatutsTable(Travel.purposes.length)
    for (let travel of travels) {
      for (let section of travel.sections) {
        let intensity = section.getCarbonIntensity(year).multiply(travel.amount)
        intensities.add(intensity)
        let index = Travel.transportations.indexOf(section.transportation)
        let indexm = Travel.purposes.indexOf(travel.purpose)
        transportationCI[index].add(intensity)
        transportationD[index] += travel.amount * section.getFullDistance()
        if (indexm !== -1) {
          totalPurposesCI[indexm].add(intensity)
          totalPurposesD[indexm] += travel.amount * section.getFullDistance()
        }
        if (travel.status) {
          statusCI[travel.status][index].add(intensity)
          statusD[travel.status][index] += travel.amount * section.getFullDistance()
          if (indexm !== -1) {
            purposesCI[travel.status][indexm].add(intensity)
            purposesD[travel.status][indexm] += travel.amount * section.getFullDistance()
          }
        }
      }
    }
    Object.keys(statusCI).map(function (key) {
      statusCI[key] = statusCI[key].map(obj => obj.sum())
    })
    Object.keys(purposesCI).map(function (key) {
      purposesCI[key] = purposesCI[key].map(obj => obj.sum())
    })
    return {
      'intensity': intensities.sum(),
      'transportations': {
        'intensity': transportationCI.map(obj => obj.sum()),
        'distances': transportationD
      },
      'purposes': {
        'intensity': totalPurposesCI.map(obj => obj.sum()),
        'distances': totalPurposesD
      },
      'status': {
        'intensity': statusCI,
        'distances': statusD
      },
      'statuspurposes': {
        'intensity': purposesCI,
        'distances': purposesD
      }
    }
  }
}
