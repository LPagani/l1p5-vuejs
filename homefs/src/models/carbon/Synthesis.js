/**********************************************************************************************************
 * Author :
 *   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
 *
 * Copyright (C) 2020
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 ***********************************************************************************************************/

import Modules from '@/models/Modules.js'
import {
  CarbonIntensity,
  CarbonIntensities
} from '@/models/carbon/CarbonIntensity.js'

/** Represent the footprint synthesis of a ghgi (travels, building). */
export default class Synthesis {
  /**
   * Create a new Synthesis
   */
  constructor (obj) {
    for (let module of Modules.getModules(true)) {
      this[module] = new CarbonIntensity()
      if (obj) {
        if (Object.keys(obj).includes(module)) {
          this[module] = CarbonIntensity.createFromObj(obj[module])
        }
      }
    }
  }

  get buildings () {
    let intensities = new CarbonIntensities()
    for (let module of Modules.MODULES[Modules.BUILDINGS]) {
      intensities.add(this[module])
    }
    return intensities.sum()
  }

  get intensity () {
    let intensity = 0.0
    // TODO restrict to modules in settings
    for (let module of Modules.getModules(true)) {
      intensity += this[module].intensity
    }
    return intensity
  }

  get uncertainty () {
    let uncertainty = 0.0
    // TODO restrict to modules in settings
    for (let module of Modules.getModules(true)) {
      uncertainty += this[module].uncertainty
    }
    return uncertainty
  }

  static createFromObj (item) {
    return new Synthesis(item)
  }
}
