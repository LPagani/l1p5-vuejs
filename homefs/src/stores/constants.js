import Modules from '@/models/Modules.js'

export const DEFAULT_SETTINGS = [
  {
    'section': 'global',
    'name': 'ACTIVE_MODULES',
    'value': Modules.getModules(),
    'type': 1,
    'component': 'ActiveModules'
  },
  {
    'section': 'global',
    'name': 'MODULES_COLORS',
    'value': Modules.DEFAULT_COLORS,
    'type': 1,
    'component': 'ModulesColors'
  },
  {
    'section': 'global',
    'name': 'GEONAMES_USERNAME',
    'value': 'labos1point5',
    'type': 1
  },
  {
    'section': 'global',
    'name': 'ALLOWED_COUNTRIES',
    'value': ['MF', 'BL', 'PF', 'YT', 'GF', 'PM', 'RE', 'GP', 'MQ', 'WF', 'TF', 'NC', 'FR'],
    'type': 1,
    'component': 'AllowedCountries'
  },
  {
    'section': 'commute',
    'name': 'NUMBER_OF_WORKED_WEEK.default',
    'value': '41',
    'type': 2
  },
  {
    'section': 'commute',
    'name': 'NUMBER_OF_WORKED_WEEK.2020',
    'value': '27',
    'type': 2
  },
  {
    'section': 'commute',
    'name': 'NUMBER_OF_WORKED_WEEK.2021',
    'value': '37',
    'type': 2
  },
  // client side only
  {
    'section': 'transition',
    'name': 'ACCEPTED_MIME_TYPES',
    'value': 'application/pdf,image/png,image.jpeg',
    'type': 3
  }
]
