/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { defineStore } from 'pinia'
import { DEFAULT_SETTINGS } from './constants'
import coreService from '@/services/coreService'
import Modules from '@/models/Modules.js'

export const useCoreStore = defineStore('core', {
  state: () => {
    return {
      settings: DEFAULT_SETTINGS
    }
  },
  getters: {
    setting: (state) => {
      return (section, name) => {
        if (section && name) {
          return state.settings.filter(obj => obj.section === section && obj.name === name)[0]
        } else if (name) {
          return state.settings.filter(obj => obj.name === name)[0]
        } else if (section) {
          return state.settings.filter(obj => obj.section === section)
        }
      }
    },
    activeModules: (state) => {
      return (includesSubModules = false) => {
        return Modules.getModules(
          includesSubModules,
          state.settings.filter(obj => obj.section === 'global' && obj.name === 'ACTIVE_MODULES')[0].value
        )
      }
    },
    modulesColors () {
      let colors = this.setting('global', 'MODULES_COLORS').value
      for (let module of this.activeModules(true)) {
        if (!Object.keys(colors).includes(module)) {
          colors[module] = Modules.DEFAULT_COLORS[module]
        }
      }
      return colors
    },
    sections: (state) => {
      return new Set(state.settings.map(obj => obj.section))
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    getSettings () {
      return coreService.getSettings({ 'settings': this.settings })
        .then(settings => {
          this.settings = settings
          return settings
        })
        .catch(error => {
          throw error
        })
    },
    saveSettings () {
      return coreService.saveSettings({ 'settings': this.settings })
        .then(settings => {
          this.settings = settings
          return settings
        })
        .catch(error => {
          throw error
        })
    },
    updateSettings (setting) {
      let csetting = this.settings.filter(obj => obj.id === setting.id)[0]
      csetting.value = setting.value
    }
  }
})
