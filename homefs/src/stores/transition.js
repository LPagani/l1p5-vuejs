import { defineStore } from 'pinia'
import transitionService from '@/services/transitionService'

export const useTransitionStore = defineStore('transition', {
  state: () => ({
    isReviewer: false,
    hasSubmittedGHGIs: false,
    // avoid to keep asking for it
    // when moving across pages
    introduction: {
      isValid: false,
      isAnonymous: false
    },
    canAddActions: false,
    filterTab: 0

  }),
  actions: {
    resetState () {
      this.$reset()
    },
    async init () {
      Promise.all([
        this.hasReviewerPermissions(),
        this.checkIfSubmittedGHGIs()
      ])
      // make sure hasSubmittedGHGIs is set
      // before proceeding
        .then(() => this.checkCanAddActions())
    },
    async hasReviewerPermissions () {
      return transitionService
        .hasReviewerPermissions()
        .then((data) => {
          this.isReviewer = data.has_reviewer_permissions
          return this.isReviewer
        })
        .catch((error) => {
          console.error(error) // eslint-disable-line no-console
          this.isReviewer = false
        })
    },
    async checkIfSubmittedGHGIs () {
      return transitionService
        .hasSubmittedGHGIs()
        .then((data) => {
          this.hasSubmittedGHGIs = data.has_submitted_ghgis
          return this.hasSubmittedGHGIs
        })
        .catch((error) => {
          console.error(error) // eslint-disable-line no-console
          this.hasSubmittedGHGIs = false
          return this.hasSubmittedGHGIs
        })
    },
    async checkCanAddActions () {
      // rationale: shortcut the introduction step
      if (!this.hasSubmittedGHGIs) {
        // we don't fulfill the initial requirement
        // so don't go further
        return false
      }

      // this.hasSubmittedGHGIS = true
      // but we aren't sure we can skip the introduction step
      // our criteria is that we can skip if there are already some actions

      if (this.canAddActions) {
        // We already have some actions, so we proceed
        // as this is sign that  the charter has been already accepted in the past
        return true
      }

      // otherwise check if there are some actions already
      // use case: the user just logged in
      // we shortcut the intro
      // the following call should be called only once per session
      transitionService.getCount()
        .catch(error => {
          switch (error.response.status) {
            case 401:
              // unauthorized => anonymous call
              return { count: 0 }
            case 403:
              // unauthorized => anonymous call
              return { count: 0 }
            case 404:
              // not found (most likely the laboratory)
              return { count: 0 }
            default:
              // escalate
              throw error
          }
        })
        .then(data => {
          if (data.count > 0) {
            // we already have some actions
            // shortcut the introduction process
            this.canAddActions = true
            this.introIsValid()
            return this.canAccessActions
          } else {
            this.canAddActions = false
            return this.canAddActions
          }
        })
    },
    introIsValid () {
      this.introduction.isValid = true
      this.introduction.isAnonymous = false
      this.canAddActions = true
    },
    introIsAnonymous () {
      this.introduction.isValid = false
      this.introduction.isAnonymous = true
      this.canAddActions = false
    },
    introIsInvalid () {
      this.introduction.isValid = false
      this.introduction.isAnonymous = false
      this.canAddActions = false
    }
  }
})
