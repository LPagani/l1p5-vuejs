import { defineStore } from 'pinia'
import { useCoreStore } from './core'
import { useAdminStore } from './admin'

import scenarioService from '@/services/scenarioService'
import Scenario from '@/models/scenario/Scenario.js'

export const useScenarioStore = defineStore('scenario', {
  state: () => ({
    item: {}
  }),
  getters: {
    settings: (state) => {
      const core = useCoreStore()
      return core.setting('commute')
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    set (uuid) {
      return scenarioService.getScenario({
        uuid: uuid
      })
        .then(scenario => {
          let module = null
          const admin = useAdminStore()
          let ghgi = admin.allGHGI.filter(obj => obj.id === scenario.ghgi.id)[0]
          this.item = Scenario.createFromObj(scenario)
          this.item.setGHGI(ghgi)
          this.item.compute(this.settings, module)
          return scenario
        })
        .catch(error => {
          throw error
        })
    },
    setEmpty () {
      this.item = new Scenario(null, null, Scenario.DEFAULT_TIMELINE, null, null, [], null)
    },
    setFromUUID (uuid) {
      return scenarioService.getScenarioFromUUID({
        uuid: uuid
      })
        .then(data => {
          let module = null
          const admin = useAdminStore()
          admin.setGHGI(data.ghgi)
          let ghgi = admin.allGHGI.filter(obj => obj.id === data.scenario.ghgi.id)[0]
          this.item = Scenario.createFromObj(data.scenario)
          this.item.setGHGI(ghgi)
          this.item.compute(this.settings, module)
          return data
        })
        .catch(error => {
          throw error
        })
    },
    setGHGI (ghgi) {
      this.item.setGHGI(ghgi)
    },
    addRule (rule) {
      this.item.addRule(rule)
    },
    removeRule (rule) {
      this.item.removeRule(rule)
    },
    updateLevel1 (data) {
      let rule2update = this.item.rules.filter(obj => obj.id === data.id)[0]
      rule2update.level1 = data.value
    },
    updateLevel2 (data) {
      let rule2update = this.item.rules.filter(obj => obj.id === data.id)[0]
      for (let param of Object.keys(data.values)) {
        rule2update.level2[param].value = data.values[param]
      }
    },
    resetLevel2 (data) {
      let rule2update = this.item.rules.filter(obj => obj.id === data.id)[0]
      rule2update.resetLevel2Values()
    },
    compute (module = null) {
      this.item.compute(this.settings, module)
    },
    save (ghgiID) {
      return scenarioService.saveScenario({
        'scenario': this.item.toDatabase(),
        'ghgi_id': ghgiID
      })
        .then(scenario => {
          let module = null
          const admin = useAdminStore()
          admin.getScenarios()
          let ghgi = admin.allGHGI.filter(obj => obj.id === scenario.ghgi.id)[0]
          this.item = Scenario.createFromObj(scenario)
          this.item.setGHGI(ghgi)
          this.item.compute(this.settings, module)
          return scenario
        })
        .catch(error => {
          throw error
        })
    },
    setName (value) {
      this.item.name = value
    },
    setDescription (value) {
      this.item.description = value
    },
    setTimeline (value) {
      this.item.timeline = value
    }
  }
})
