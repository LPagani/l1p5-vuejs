/**********************************************************************************************************
* Author :
*   Jerome Mariette, INRAE, UR875 Mathématiques et Informatique Appliquées Toulouse, F-31326 Castanet-Tolosan, France
*
* Copyright (C) 2020
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************************************************/

import { defineStore } from 'pinia'
import { useCoreStore } from './core'

import scenarioService from '@/services/scenarioService'
import carbonService from '@/services/carbonService'
import coreService from '@/services/coreService'

import Scenario from '@/models/scenario/Scenario.js'
import GHGI from '@/models/carbon/GHGI.js'

import _ from 'lodash'

function updateAllGHGI (ghgis) {
  let allGHGI = []
  for (let ghgi of ghgis) {
    allGHGI.unshift(GHGI.createFromObj(ghgi))
  }
  return allGHGI
}

export const useAdminStore = defineStore('admin', {
  state: () => ({
    laboratory: {},
    allGHGI: [],
    allScenarios: [],
    allVehicles: [],
    allBuildings: []
  }),
  getters: {
    settings: (state) => {
      const core = useCoreStore()
      return core.setting('commute')
    }
  },
  actions: {
    resetState () {
      this.$reset()
    },
    getLaboratory () {
      return coreService.getLaboratory()
        .then(data => {
          if (data) {
            data['sites'] = data.sites.map(function (obj) { return obj.name })
            this.laboratory = data
            return data
          }
          return null
        })
        .catch(error => {
          throw error
        })
    },
    saveLaboratory (laboratory) {
      return coreService.saveLaboratory(laboratory)
        .then(data => {
          data['sites'] = data.sites.map(function (obj) { return obj.name })
          this.laboratory = data
          return data
        })
        .catch(error => {
          throw error
        })
    },
    updateLaboratory (laboratory) {
      this.laboratory = laboratory
    },
    deleteGHGI (id) {
      return carbonService.deleteGHGI({ 'ghgi_id': id })
        .then(() => {
          // remove in-place the current ghgi from the current list
          this.allGHGI = _.reject(this.allGHGI, ghgi => ghgi.id === id)
          return this.allGHGI
        })
        .catch(error => {
          throw error
        })
    },
    getAllGHGI () {
      return carbonService.getAllGHGI()
        .then(ghgis => {
          this.allGHGI = updateAllGHGI(ghgis)
          this.computeAllGHGI()
          return ghgis
        })
        .catch(error => {
          throw error
        })
    },
    getAllGHGIWithoutConsumptions () {
      return carbonService.getAllGHGIWithoutConsumptions()
        .then(ghgis => {
          this.allGHGI = updateAllGHGI(ghgis)
          return ghgis
        })
        .catch(error => {
          throw error
        })
    },
    computeAllGHGI (year = null) {
      for (let ghgi of this.allGHGI) {
        ghgi.compute(this.settings, year)
      }
    },
    setGHGI (ghgi) {
      this.allGHGI = updateAllGHGI([ghgi])
      this.computeAllGHGI()
    },
    updateGHGIYear (id, year) {
      let ghgis = this.allGHGI.filter(obj => obj.id === id)
      if (ghgis.length === 1) {
        ghgis[0].year = year
      }
    },
    getScenarios () {
      return scenarioService.getScenarios()
        .then(scenarios => {
          this.setScenarios(scenarios)
          return scenarios
        })
        .catch(error => {
          throw error
        })
    },
    setScenarios (scenarios) {
      this.allScenarios = []
      for (let scenario of scenarios) {
        this.allScenarios.unshift(Scenario.createFromObj(scenario))
      }
    },
    deleteScenario (id) {
      return scenarioService.deleteScenario({
        id: id
      })
        .then(scenarios => {
          this.setScenarios(scenarios)
          return scenarios
        })
        .catch(error => {
          throw error
        })
    },
    getAllVehicles () {
      return carbonService.getAllVehicles()
        .then(data => {
          if (data) {
            this.allVehicles = data
          }
          return data
        })
        .catch(error => {
          throw error
        })
    },
    getAllBuildings () {
      return carbonService.getAllBuildings()
        .then(data => {
          if (data) {
            this.allBuildings = data
          }
          return data
        })
        .catch(error => {
          throw error
        })
    }
  }
})
