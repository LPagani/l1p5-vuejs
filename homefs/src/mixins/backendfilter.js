import coreService from '@/services/coreService'
import { TAGS } from '@/models/transition/transition.js'
import { buildDisciplinesTree } from '@/models/transition/utils.js'

import { Background } from '@/mixins/background.js'

/**
 * Common filtering logic
 */
export const BackendFilter = {
  props: {
    fullscreen: {
      type: Boolean,
      required: false,
      default: true
    },
    ItemClass: {
      type: Function,
      required: true
    },
    getNextFnc: {
      type: Function,
      required: true
    },
    getItems: {
      type: Function,
      required: true
    },
    openItem: {
      type: Function,
      required: true
    },
    clickFile: {
      type: Function,
      require: true
    }
  },
  mixins: [Background],
  data: function () {
    return {
      availableTags: TAGS,
      filteredTags: TAGS,
      disciplinesTree: [],
      filters: {},
      items: [],
      currentItem: null,
      next: null,
      count: 0,
      localPageSize: 50,
      localPage: 1
    }
  },
  computed: {
    displayedItems () {
      return this.items.slice(0, this.localPage * this.localPageSize)
    }
  },
  methods: {
    itemOnMouseOver (item) {},
    filtered (filters) {
      Object.assign(this.filters, filters)
      this.filterAll()
    },
    itemOnClick (item) {
      let route = this.$router.resolve(this.openItem(item.id))
      window.open(route.href, '_blank')
    },
    loadMore () {
      if (this.next !== null) {
        // fetch new batch of data from remote server
        return this.getNextFnc(this.next)
          .then((data) => {
            this.items = this.items.concat(
              data.results.map((a) => this.ItemClass().fromDatabase(a))
            )
            this.next = data.next
            this.count = data.count
            this.localPage = this.localPage + 1
          })
          .then(() => this.afterFilterAll())
      } else {
        // the data is already around, just increase the window
        this.localPage = this.localPage + 1
      }
    },
    filterAll () {
      // make it fully async
      this.getItems(this.filters)
        .then((data) => {
          this.items = data.results.map((a) => this.ItemClass().fromDatabase(a))
          this.next = data.next
          this.count = data.count
        })
        .then(() => this.afterFilterAll())
    },
    boundsUpdated (bounds) {
      let [lat1, lon1] = [bounds._southWest.lat, bounds._southWest.lng]
      let [lat2, lon2] = [bounds._northEast.lat, bounds._northEast.lng]
      this.filters.bounds = [
        [lat1, lon1],
        [lat2, lon2]
      ]
      this.filterAll()
    }
  },
  mounted () {
    this.bgUI(async () => {
      let disciplines = await coreService.getDisciplines()
      this.disciplinesTree = buildDisciplinesTree(disciplines)
      await this.filterAll()
    })
  }
}
