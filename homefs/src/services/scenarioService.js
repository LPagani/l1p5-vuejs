import api from '@/services/api'

export default {
  saveScenario (payload) {
    return api.post(`save_scenario/`, payload)
      .then(response => response.data)
  },
  getScenario (payload) {
    return api.post(`get_scenario/`, payload)
      .then(response => response.data)
  },
  getScenarioFromUUID (payload) {
    return api.post(`get_scenario_from_uuid/`, payload)
      .then(response => response.data)
  },
  getScenarios () {
    return api.post(`get_scenarios/`)
      .then(response => response.data)
  },
  deleteScenario (payload) {
    return api.post(`delete_scenario/`, payload)
      .then(response => response.data)
  },
  getScenariosAdmin (payload) {
    return api.post(`get_scenarios_admin/`, payload)
      .then(response => response.data)
  }
}
