import { parseFile } from './travelsCSVParser.js'
import { readFile } from 'fs/promises'
const path = require('node:path')

// see https://framagit.org/labos1point5/l1p5-vuejs/-/issues/155
let fromGeslab = `Groupe labo%Numéro mission%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Moyens de transport%Nb de pers. dans la voiture%Aller / Retour%Motif du déplacement%Statut agent%Date de retour
0000%17713%2022-01-03%ORSAY%France%SANTA BARBARA%Etats-Unis%Avion%%OUI%Autres%%2022-01-28
0000%17657%2022-01-08%PETIT MARS%France%AUSSOIS%France%Taxi,Train%1%OUI%Colloques et congrés%%2022-01-15`

// excerpt from public/static/carbon/travelsTemplate.tsv
let fromLabos1point5FR = `# mission%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Mode de déplacement%Nb de personnes dans la voiture%Aller Retour%Motif du déplacement%Statut de l'agent
1%24/01/2019%Grenoble%France%Lyon Saint-Exupéry%France%bus%%OUI%Colloque-congrès%ITA
1%24/01/2019%Lyon Saint Exupéry%FR%Londres%GB%avion%%OUI%Colloque-congrès%ITA
2%24/01/2019%Grenoble%France%Marseille%France%voiture%4%OUI%Colloque-congrès%ITA`

// same but with Engish columns names
let fromLabos1point5EN = `# trip%Departure date%Departure city%Country of departure%Destination city%Country of destination%Mode of transportation%No of persons in the car%Roundtrip%Purpose of the trip%Agent position
1%24/01/2019%Grenoble%France%Lyon Saint-Exupéry%France%bus%%OUI%Colloque-congrès%ITA
1%24/01/2019%Lyon Saint Exupéry%FR%Londres%GB%avion%%OUI%Colloque-congrès%ITA
2%24/01/2019%Grenoble%France%Marseille%France%voiture%4%OUI%Colloque-congrès%ITA`

function doParseLabo1point5 (file) {
  // let data = parseTravels(txt, 'fr')
  parseFile(file, 'fr')
    .then((data) => {
      expect(data.length).toEqual(3)

      expect(data[0]).toStrictEqual({
        amount: 1,
        carpooling: 1,
        date: '24/01/2019',
        departureCity: 'Grenoble',
        departureCountry: 'FR',
        destinationCity: 'Lyon Saint-Exupéry',
        destinationCountry: 'FR',
        isRoundTrip: 'OUI',
        names: ['1'],
        purpose: 'Colloque-congrès',
        status: 'ITA',
        transportation: 'bus'
      })
      // the is the second section of the 1st mission
      expect(data[1]).toStrictEqual({
        amount: 1,
        carpooling: 1,
        date: '24/01/2019',
        departureCity: 'Lyon Saint Exupéry',
        departureCountry: 'FR',
        destinationCity: 'Londres',
        destinationCountry: 'GB',
        isRoundTrip: 'OUI',
        names: ['1'],
        purpose: 'Colloque-congrès',
        status: 'ITA',
        // NOTE(msimonin): 'avion' is used here instead of plane for geslab. the
        // fact that avion == plane is handled later in the stack by using
        // models.carbon.MODE_DICTIONARY
        transportation: 'avion'
      })

      // The second mission
      expect(data[2]).toStrictEqual({
        amount: 1,
        carpooling: 4,
        date: '24/01/2019',
        departureCity: 'Grenoble',
        departureCountry: 'FR',
        destinationCity: 'Marseille',
        destinationCountry: 'FR',
        isRoundTrip: 'OUI',
        names: ['2'],
        purpose: 'Colloque-congrès',
        status: 'ITA',
        // NOTE(msimonin): 'avion' is used here instead of plane for geslab. the
        // fact that avion == plane is handled later in the stack by using
        // models.carbon.MODE_DICTIONARY
        transportation: 'voiture'
      })
    })
}

describe('parseTravels', () => {
  test('travelsTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/travelsTemplate.csv'
      ),
      'utf8'
    )
    parseFile(datafile, 'fr')
      .then((data) => {
        expect(data.length).toBe(23)
        expect(Object.keys(data[0]).length).toBe(12)
      })
  })

  test('travelsTemplate.tsv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/travelsTemplate.tsv'
      ),
      'utf8'
    )
    parseFile(datafile, 'fr')
      .then((data) => {
        expect(data.length).toBe(23)
        expect(Object.keys(data[0]).length).toBe(12)
      })
  })

  test('geslab import', () => {
    // usual case: \t as separator
    let txt = fromGeslab.replace(/%/g, '\t')

    parseFile(txt, 'fr')
      .then((data) => {
        // NOTE the returned data is more than an array of objects it has a columns
        // attribute which prevents us to use toStrictEqual on the whole data.
        expect(data.length).toEqual(2)
        expect(data[0]).toStrictEqual({
          amount: 1,
          carpooling: 1,
          date: '2022-01-03',
          departureCity: 'ORSAY',
          departureCountry: 'FR',
          destinationCity: 'SANTA BARBARA',
          destinationCountry: 'US',
          isRoundTrip: 'OUI',
          names: ['17713'],
          purpose: 'other',
          status: '',
          transportation: 'plane'
        })
        expect(data[1]).toStrictEqual({
          amount: 1,
          carpooling: 1,
          date: '2022-01-08',
          departureCity: 'PETIT MARS',
          departureCountry: 'FR',
          destinationCity: 'AUSSOIS',
          destinationCountry: 'FR',
          isRoundTrip: 'OUI',
          names: ['17657'],
          purpose: 'conference',
          status: '',
          transportation: 'train'
        })
      })
  })

  test('geslab import with wrong separator throws an exception', () => {
    // ; as a separator
    let txt = fromGeslab.replace(/%/g, ';')
    expect(parseFile(txt, 'fr'))
      .rejects
      .toThrow(new Error('error-msg-csv-geslabsep'))
  })

  test('[fr] Labos1point5 import with tabs', () => {
    let txt = fromLabos1point5FR.replace(/%/g, '\t')
    doParseLabo1point5(txt)
  })

  test('[fr] Labos1point5 import with comma', () => {
    let txt = fromLabos1point5FR.replace(/%/g, ',')
    doParseLabo1point5(txt)
  })

  test('[fr] Labos1point5 import with semicolon', () => {
    let txt = fromLabos1point5FR.replace(/%/g, ';')
    doParseLabo1point5(txt)
  })

  test('[en] Labos1point5 import with tabs', () => {
    let txt = fromLabos1point5EN.replace(/%/g, '\t')
    doParseLabo1point5(txt)
  })

  test('[en] Labos1point5 import with comma', () => {
    let txt = fromLabos1point5EN.replace(/%/g, ',')
    doParseLabo1point5(txt)
  })

  test('[en] Labos1point5 import with semicolon', () => {
    let txt = fromLabos1point5EN.replace(/%/g, ';')
    doParseLabo1point5(txt)
  })

  test('[fr] Labos1point5 import no hyphen', () => {
    let txt = fromLabos1point5FR.replace('é', 'e').replace(/%/g, ';')
    doParseLabo1point5(txt)
  })

  // test('travelsTemplate.tsv', () => {
  //   let fs = require('fs')
  //   let path = require('path')
  //   fs.readFile(
  //     path.join(__dirname, '../../public/static/carbon/travelsTemplate.tsv'),
  //     'utf8',
  //     function (_, txt) {
  //       let data = parseTravels(txt, 'fr')
  //       expect(data.length).toEqual(23)
  //     }
  //   )
  // })

  /**
   * Intent: check that the name attribute of the error is localized
   *
   */
  test('Missing mandatory field throw an exception', () => {
    async function testError (txt, locale, errcode, errparams) {
      // first check that the error is thrown
      expect(parseFile(txt, locale)).rejects.toThrow(new Error(errcode))
      // second test that the params of the error is set correctly
      try {
        await parseFile(txt, locale)
      } catch (e) {
        expect(e.params).toStrictEqual(errparams)
      }
    }

    // geslab format
    let geslabData =
      'Groupe labo%not-a-valid-field%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Moyens de transport%Nb de pers. dans la voiture%Aller / Retour%Motif du déplacement%Statut agent%Date de retour'
    let gestlabTxt = geslabData.replace(/%/g, '\t')
    testError(gestlabTxt, 'fr', 'error-msg-csv-missing', {
      name: 'Numéro mission'
    })
    testError(gestlabTxt, 'en', 'error-msg-csv-missing', { name: '# trip' })

    // labo1point5 format (yes we can upload a french file but be en-localized)
    let l1p5Data = `not-a-valid-field%Date de départ%Ville de départ%Pays de départ%Ville de destination%Pays de destination%Mode de déplacement%Nb de personnes dans la voiture%Aller Retour%Motif du déplacement%Statut de l'agent`
    let l1p5Txt = l1p5Data.replace(/%/g, '\t')
    testError(l1p5Txt, 'fr', 'error-msg-csv-missing', { name: '# mission' })
    testError(l1p5Txt, 'en', 'error-msg-csv-missing', { name: '# trip' })

    // Note(msimonin). In english 'Roundtrip' matches both the names.pattern and the isRoundTrip pattern
    // so trying to call with the following won't throw any Exception
    // In other word a file with missing (# trip) field will be valid (like the following)
    // let l1p5DataEn = `not-a-valid-field%Departure date%Departure city%Country of departure%Destination city%Country of destination%Mode of transportation%No of persons in the car%Roundtrip%Purpose of the trip%Agent position`
    //
    // use another field as missing field
    let l1p5DataEn = `# trip%not-a-valid-field%Departure city%Country of departure%Destination city%Country of destination%Mode of transportation%No of persons in the car%Roundtrip%Purpose of the trip%Agent position`
    let l1p5TxtEn = l1p5DataEn.replace(/%/g, '\t')
    testError(l1p5TxtEn, 'fr', 'error-msg-csv-missing', {
      name: 'Date de départ'
    })
    testError(l1p5TxtEn, 'en', 'error-msg-csv-missing', {
      name: 'Departure date'
    })
  })
})
