import { parseFile } from './vehiclesParser'
import { readFile } from 'fs/promises'
const path = require('node:path')

const header = 'Identifiant\tType\tMotorisation\tDistance'

describe('parseVehicles', () => {
  test('vehiclesTemplate.csv', async () => {
    let datafile = await readFile(
      path.resolve(
        __dirname,
        '../../',
        'public/static/carbon/vehiclesTemplate.tsv'
      ),
      'utf8'
    )
    parseFile(datafile)
      .then(([parsedData, headerMap, status]) => {
        expect(parsedData.length).toBe(9)
        expect(headerMap).toStrictEqual({
          distance: 'Distance',
          engine: 'Motorisation',
          name: 'Identifiant',
          type: 'Type'
        })
        expect(status).toBe('ok')
      })
  })

  test('Empty data', async () => {
    parseFile(header)
      .then(([parsedData, headerMap, status]) => {
        expect(parsedData.length).toBe(0)
        expect(headerMap).toStrictEqual({
          distance: 'Distance',
          engine: 'Motorisation',
          name: 'Identifiant',
          type: 'Type'
        })
        expect(status).toBe('ok')
      })
  })
})
