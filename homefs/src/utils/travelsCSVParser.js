
import Travel from '@/models/carbon/Travel.js'
import * as Papa from 'papaparse'

function intConverter (defaultValue) {
  return function (inValue) {
    let value = defaultValue
    if (!isNaN(inValue) && inValue !== '') {
      value = parseInt(inValue)
    }
    return value
  }
}

const columnDescriptions = {
  names: {
    en: '# trip',
    fr: '# mission',
    geslab: 'Numéro mission',
    pattern: /(# |Num.ro |)(mission|trip)/i,
    converter: x => [x] },
  date: { en: 'Departure date', fr: 'Date de départ' },
  departureCity: { en: 'Departure city', fr: 'Ville de départ' },
  departureCountry: { en: 'Country of departure', fr: 'Pays de départ', converter: Travel.iso3166Country },
  destinationCity: { en: 'Destination city', fr: 'Ville de destination' },
  destinationCountry: { en: 'Country of destination', fr: 'Pays de destination', converter: Travel.iso3166Country },
  transportation: {
    en: 'Mode of transportation ',
    fr: 'Mode de déplacement',
    geslab: 'Moyens de transport',
    pattern: /(mode|moyens).*(transport|d.placement)/i },
  carpooling: {
    en: 'No of persons in the car',
    fr: 'Nb de personnes dans la voiture',
    default: 1,
    geslab: 'Nb de pers. dans la voiture',
    converter: intConverter(1)
  },
  isRoundTrip: { en: 'Roundtrip', fr: 'Aller / Retour', pattern: /(Roundtrip|Aller.*Retour)/i },
  purpose: {
    en: 'Purpose of the trip',
    fr: 'Motif du déplacement',
    optional: true,
    default: ''
  },
  status: {
    en: 'Agent position',
    fr: 'Statut de l\'agent',
    pattern: /(Agent position|Statut.*agent)/i,
    optional: true,
    default: ''
  },
  amount: {
    en: 'Amount',
    fr: 'Quantité',
    pattern: /(quantit.|nombre|amount|number)/i,
    optional: true,
    default: 1,
    converter: intConverter(1)
  }
}

function _convertGeslabModeDeplacement (transportation) {
  let finalMode = ''
  if (transportation === 'Avion') {
    finalMode = Travel.MODE_PLANE
  } else if (transportation === 'Bateau') {
    finalMode = Travel.MODE_FERRY
  } else if (transportation === 'Bus') {
    finalMode = Travel.MODE_BUS
  } else if (transportation === 'Divers') {
    throw new Error('error-msg-csv-divers')
  } else if (transportation === 'Location de véhicule') {
    finalMode = Travel.MODE_CAR
  } else if (transportation === 'Metro') {
    finalMode = Travel.MODE_SUBWAY
  } else if (transportation === 'Passager') {
    throw new Error('error-msg-csv-passager')
  } else if (transportation === 'Rer') {
    finalMode = Travel.MODE_RER
  } else if (transportation === 'Taxi') {
    finalMode = Travel.MODE_CAB
  } else if (transportation === 'Train') {
    finalMode = Travel.MODE_TRAIN
  } else if (transportation === 'Véhicule administratif') {
    throw new Error('error-msg-csv-admin')
  } else if (transportation === 'Véhicule personnel') {
    finalMode = Travel.MODE_CAR
  }
  return finalMode
}

function _getDominantModeDeplacement (allModes) {
  let finalMode = ''
  let modesOrder = [
    Travel.MODE_PLANE,
    Travel.MODE_TRAIN,
    Travel.MODE_FERRY,
    Travel.MODE_BUS,
    Travel.MODE_CAR,
    Travel.MODE_RER,
    Travel.MODE_SUBWAY,
    Travel.MODE_CAB
  ]
  for (let mode of modesOrder) {
    if (allModes.includes(mode)) {
      finalMode = mode
      break
    }
  }
  return finalMode
}

function _getGestlabModeDeplacement (transportation) {
  let allModes = transportation.split(',')
  let finalMode = ''
  if (allModes.length === 1) {
    finalMode = _convertGeslabModeDeplacement(transportation)
  } else {
    let convertedAllModes = allModes.map(value => {
      try {
        return _convertGeslabModeDeplacement(value)
      } catch (e) {
        return ''
      }
    })
    // test if only blank value obtained from catch
    let nbBlank = 0
    for (let value of convertedAllModes) {
      if (value === '') {
        nbBlank += 1
      }
    }
    if (nbBlank === convertedAllModes.length) {
      let err = new Error('error-msg-csv-unknown')
      err.params = { 'transportation': transportation }
      throw err
    }
    finalMode = _getDominantModeDeplacement(convertedAllModes)
  }
  finalMode = Travel.getTransportation(finalMode)
  if (finalMode === null) {
    let err = new Error('error-msg-csv-unknown')
    err.params = { 'transportation': transportation }
    throw err
  }
  return finalMode
}

function _getGeslabMotifDeplacement (motifDeplacement) {
  let md = Travel.PURPOSE_UNKNOWN
  if (motifDeplacement === 'Acq nouv connaissances&techniq') {
    md = Travel.PURPOSE_SEMINAR
  } else if (motifDeplacement === 'Administration de la recherche') {
    md = Travel.PURPOSE_RESEARCH_MANAGEMENT
  } else if (motifDeplacement === 'Autres') {
    md = Travel.PURPOSE_OTHER
  } else if (motifDeplacement === 'Colloques et congrés') {
    md = Travel.PURPOSE_CONFERENCE
  } else if (motifDeplacement === 'Enseignement dispensé') {
    md = Travel.PURPOSE_TEACHING
  } else if (motifDeplacement === 'Rech. doc. ou sur terrain') {
    md = Travel.PURPOSE_FIELD_STUDY
  } else if (motifDeplacement === 'Rech. en équipe, collaboration') {
    md = Travel.PURPOSE_COLLABORATION
  } else if (motifDeplacement === 'Visite, contact pour projet') {
    md = Travel.PURPOSE_VISIT
  }
  return md
}

function parseTravels (results, locale) {
  var [parsedData, headerFields, sep, errors] = results
  var outData = []
  if (errors === 'ok') {
    let headerMap = {}
    let labels = ['en', 'fr', 'geslab']
    let isGeslab = headerFields.includes('Groupe labo')
    if (isGeslab && sep !== '\t') {
      throw new Error('error-msg-csv-geslabsep')
    }
    Object.entries(columnDescriptions).forEach(function ([outName, desc]) {
      let pattern = ''
      if ('pattern' in desc) {
        pattern = desc.pattern
      } else {
        pattern = new RegExp('(' +
          Object.entries(desc)
            .filter(k => labels.includes(k[0]))
            .map(k => k[1].replace(/[éèë]/, '.'))
            .join('|') +
          ')', 'i')
      }
      let matchedName
      for (let inName of headerFields) {
        if (inName.search(pattern) >= 0) {
          matchedName = inName
          break
        }
      }
      if ((!('optional' in desc && desc.optional)) && (!matchedName)) {
        let name = ''
        if (locale === 'fr') {
          name = isGeslab && 'geslab' in desc ? desc.geslab : desc.fr
        } else {
          name = desc.en
        }
        let err = new Error('error-msg-csv-missing')
        err.params = { 'name': name }
        throw err
      }
      headerMap[outName] = {
        inName: matchedName,
        default: desc.default,
        converter: 'converter' in desc ? desc.converter : x => x
      }
    })
    if (isGeslab) {
      headerMap.transportation.converter = _getGestlabModeDeplacement
      headerMap.purpose.converter = _getGeslabMotifDeplacement
    }
    for (let travel of parsedData) {
      let outTravel = {}
      Object.entries(headerMap).forEach(function ([outName, params]) {
        if (params.inName) {
          let value = travel[params.inName].replace('"', '').trim()
          try {
            outTravel[outName] = params.converter(value)
          } catch (error) {
            outTravel[outName] = value
          }
          if (!value && 'default' in params) {
            outTravel[outName] = params.default
          }
        } else if ('default' in params) {
          outTravel[outName] = params.default
        } else {
          outTravel[outName] = ''
        }
      })
      outData.push(outTravel)
    }
  } else {
    // TODO improve management of papaparse returned errors
    // https://www.papaparse.com/docs#errors
    if (errors[0].type === 'Delimiter') {
      throw new Error('error-msg-csv-separator')
    } else {
      throw new Error('error-msg-generic')
    }
  }
  return outData
}

const parseFile = (file, locale) => new Promise((resolve, reject) => {
  Papa.parse(file, {
    header: true,
    worker: true,
    delimitersToGuess: [',', '\t', ';'],
    skipEmptyLines: true,
    complete: function (result) {
      let errors = 'ok'
      if (result.errors.length > 0) {
        errors = result.errors
      }
      try {
        let data = parseTravels([result.data, result.meta.fields, result.meta.delimiter, errors], locale)
        resolve(data)
      } catch (error) {
        reject(error)
      }
    },
    error: function (error) {
      reject(error)
    }
  })
})

export { parseFile }
