import { test, expect } from '@playwright/test'
import {
  anonymousConnection,
  uploadBuildings,
  uploadDevices,
  uploadPurchases,
  uploadTravels,
  uploadVehicles
} from './utils'

import { join } from 'path'

test('[anonymous] upload building template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadBuildings({ page })
  await expect(page.getByTestId('buildings-table')).toHaveScreenshot(
    'buildings-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload devices template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadDevices({ page })

  await expect(page.getByTestId('devices-table')).toHaveScreenshot(
    'devices-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload purchases template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadPurchases({ page })
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot(
    'purchases-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload vechicles template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadVehicles({ page })
  await expect(page.getByTestId('vehicles-table')).toHaveScreenshot(
    'vehicles-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]upload travels template file', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadTravels({ page })
  await expect(page.getByTestId('travels-table')).toHaveScreenshot(
    'travels-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[anonymous]synthesis', async ({ page }) => {
  await anonymousConnection({ page })
  await uploadPurchases({ page })
  await uploadDevices({ page })
  await uploadVehicles({ page })
  await uploadBuildings({ page })
  await uploadTravels({ page })
  await page.locator('a').filter({ hasText: 'Empreinte carbone & soumission' }).click();
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-synhesis.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})
