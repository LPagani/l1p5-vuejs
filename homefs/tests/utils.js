export async function createGHGI ({ page }) {
  await page.getByRole('combobox').selectOption('2022')
  await page.getByPlaceholder('Budget annuel *').click()
  await page.getByPlaceholder('Budget annuel *').fill('10000')
  await page.locator('input[name="nResearcher"]').click()
  await page.locator('input[name="nResearcher"]').fill('10')
  await page.locator('input[name="nProfessor"]').click()
  await page.locator('input[name="nProfessor"]').fill('10')
  await page.locator('input[name="nEngineer"]').click()
  await page.locator('input[name="nEngineer"]').fill('10')
  await page.locator('input[name="nStudent"]').click()
  await page.locator('input[name="nStudent"]').fill('10')

  // save label depends whether the user is authenticated or not
  await page.getByTestId('ghgi-boundary-save').click()
}

export async function anonymousConnection ({ page }) {
  await page.goto('http://localhost:8080/ges-1point5')
  await page
    .locator('label')
    .filter({
      hasText:
        "J'affirme avoir pris connaissance de la Charte de Labos 1point5."
    })
    .locator('span')
    .first()
    .click()
  await page.getByRole('button', { name: 'Commencer' }).click()
  await createGHGI({ page })
}

export async function authenticatedConnection ({ page }) {
  await page.goto('http://localhost:8080/')
  await page.locator('.logbox > a').click()
  await page.getByRole('textbox', { name: 'Email *' }).click()
  await page
    .getByRole('textbox', { name: 'Email *' })
    .fill('l1p5-test-lab-0@l1p5.org')
  await page.getByRole('textbox', { name: 'Mot de passe *' }).fill('l1p5-test')
  await page.getByRole('button', { name: ' Se connecter' }).click()
  await page.getByRole('button', { name: ' Ajouter' }).click()
  await page
    .locator('label')
    .filter({
      hasText:
        "J'affirme avoir pris connaissance de la Charte de Labos 1point5."
    })
    .locator('span')
    .first()
    .click()
  await page
    .locator('label')
    .filter({
      hasText:
        "J'affirme avoir pris connaissance de la Politique de confidentialité de Labos"
    })
    .locator('span')
    .first()
    .click()
  await page
    .locator('label')
    .filter({ hasText: 'Je suis habilité.e par la' })
    .locator('span')
    .first()
    .click()
  await page.getByRole('button', { name: ' Commencer' }).click()

  await createGHGI({ page })
}

export async function noMoreLoading ({ page }) {
  // there's a loading spin that appears when saving, make sure it disappeared before proceeding
  let loading = await page.locator('.loading-overlay')
  let count = await loading.count()
  for (let i = 0; i < count; i++) {
    await loading.nth(i).waitFor({ state: 'hidden' })
  }
}

export async function uploadBuildings ({ page }) {
  await page.locator('a').filter({ hasText: 'Bâtiments' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles('public/static/carbon/buildingTemplate.tsv')
  await page.getByTestId('buildings-save').click()
  await noMoreLoading({ page })
}

export async function uploadDevices ({ page }) {
  await page.locator('a').filter({ hasText: 'Matériel informatique' }).click()
  await page
    .getByTestId('devices-upload')
    .setInputFiles('public/static/carbon/devicesTemplate.tsv')
  // Accept the warning due to the number of screens
  await page.getByRole('button', { name: 'OK' }).click()
  await page.getByTestId('devices-save').click()
  await noMoreLoading({ page })
}

export async function uploadPurchases ({ page }) {
  await page.locator('a').filter({ hasText: 'Achats' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles('public/static/carbon/purchasesTemplate.tsv')
  await page.getByTestId('purchases-save').click()
  await noMoreLoading({ page })
}

export async function uploadVehicles ({ page }) {
  await page.locator('a').filter({ hasText: 'Véhicules' }).click()
  // For file upload, give either absolute path of path relative to homefs/
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv ou .csv)' })
    .setInputFiles('public/static/carbon/vehiclesTemplate.tsv')
  await page.getByTestId('vehicles-save').click()
  await noMoreLoading({ page })
}


export async function uploadTravels ({ page }) {
  await page.getByTestId('menu-travels').click()
  await page
    .getByRole('textbox', { name: 'Téléverser un fichier (.tsv or .csv)' })
    .setInputFiles('public/static/carbon/travelsTemplate.tsv')
  await page.getByTestId('travels-save').click()
  await noMoreLoading({ page })
}

export async function carbonFootprint( {page} ) {
  await page.locator('a').filter({ hasText: 'Empreinte carbone & soumission' }).click();
  
}
