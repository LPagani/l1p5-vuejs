import { test, expect } from '@playwright/test'
import {
  authenticatedConnection,
  uploadBuildings,
  uploadDevices,
  uploadPurchases,
  uploadVehicles,
  uploadTravels
} from './utils'

import { join } from 'path'

test('[authenticated] upload building template file', async ({ page }) => {
  await authenticatedConnection({ page })
  await uploadBuildings({ page })
  await expect(page.getByTestId('buildings-table')).toHaveScreenshot(
    'buildings-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[authenticated]upload devices template file', async ({ page }) => {
  await authenticatedConnection({ page })
  await uploadDevices({ page })

  await expect(page.getByTestId('devices-table')).toHaveScreenshot(
    'devices-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[authenticated]upload purchases template file', async ({ page }) => {
  await authenticatedConnection({ page })
  await uploadPurchases({ page })
  await expect(page.getByTestId('purchases-table')).toHaveScreenshot(
    'purchases-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[authenticated]upload vechicles template file', async ({ page }) => {
  await authenticatedConnection({ page })
  await uploadVehicles({ page })
  await expect(page.getByTestId('vehicles-table')).toHaveScreenshot(
    'vehicles-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[authenticated]upload travels template file', async ({ page }) => {
  await authenticatedConnection({ page })
  await uploadTravels({ page })
  await expect(page.getByTestId('travels-table')).toHaveScreenshot(
    'travels-table.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})

test('[authenticated]synthesis', async ({ page }) => {
  await authenticatedConnection({ page })
  await uploadPurchases({ page })
  await uploadDevices({ page })
  await uploadVehicles({ page })
  await uploadBuildings({ page })
  await uploadTravels({ page })
  await page.locator('a').filter({ hasText: 'Empreinte carbone & soumission' }).click();
  await expect(page.getByTestId('ghgi-synthesis')).toHaveScreenshot(
    'ghgi-synhesis.png',
    { stylePath: join(__dirname, 'screenshot.css') }
  )
})
